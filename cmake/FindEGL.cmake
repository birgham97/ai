find_package (PkgConfig)
pkg_check_modules (PC_EGL QUIET EGL)
set (EGL_DEFINITIONS ${PC_EGL_CFLAGS_OTHER})

find_path (EGL_INCLUDE_DIR EGL/egl.h
           HINTS ${PC_EGL_INCLUDEDIR} ${PC_EGL_INCLUDE_DIRS} $ENV{EGL_DIR}/include
           PATH_SUFFIXES libEGL)

find_library (EGL_LIBRARY NAMES EGL libEGL
              HINTS ${PC_EGL_LIBDIR} ${PC_EGL_LIBRARY_DIRS} $ENV{EGL_DIR}/lib)

include (FindPackageHandleStandardArgs)

find_package_handle_standard_args (EGL DEFAULT_MSG
                                   EGL_LIBRARY EGL_INCLUDE_DIR)

mark_as_advanced (EGL_INCLUDE_DIR EGL_LIBRARY)

set (EGL_LIBRARIES ${EGL_LIBRARY})
set (EGL_INCLUDE_DIRS ${EGL_INCLUDE_DIR})

add_library (EGL UNKNOWN IMPORTED)
set_target_properties (EGL PROPERTIES IMPORTED_LOCATION ${EGL_LIBRARY})
set_target_properties (EGL PROPERTIES INTERFACE_INCLUDE_DIRECTORIES ${EGL_INCLUDE_DIRS})
