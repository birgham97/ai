
if (MXNet_USE_STATIC_LIBS)
  set (MXNet_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES})
  if (WIN32)
    set (CMAKE_FIND_LIBRARY_SUFFIXES .lib .a ${CMAKE_FIND_LIBRARY_SUFFIXES})
  else ()
    set (CMAKE_FIND_LIBRARY_SUFFIXES .a)
  endif ()
endif ()

if (DEFINED ENV{MXNet_DIR})
  set (MXNet_DIR $ENV{MXNet_DIR} CACHE PATH "")
endif ()

find_path (MXNet_C_INCLUDE_DIR mxnet/base.h
           HINTS ${MXNet_DIR}/include)

find_path (MXNet_CPP_INCLUDE_DIR mxnet-cpp/base.h
           HINTS ${MXNet_DIR}/cpp-package/include)

find_path (NNVM_INCLUDE_DIR nnvm/base.h
           HINTS ${MXNet_DIR}/3rdparty/tvm/nnvm/include)

find_path (DMLC_INCLUDE_DIR dmlc/base.h
           HINTS ${MXNet_DIR}/3rdparty/dmlc-core/include)

find_library (MXNet_LIBRARY NAMES mxnet libmxnet
              HINTS ${MXNet_DIR}/lib ${MXNet_DIR}/build)

include (FindPackageHandleStandardArgs)

find_package_handle_standard_args (MXNet DEFAULT_MSG
                                   MXNet_LIBRARY MXNet_C_INCLUDE_DIR MXNet_CPP_INCLUDE_DIR
                                   NNVM_INCLUDE_DIR DMLC_INCLUDE_DIR)

mark_as_advanced (MXNet_C_INCLUDE_DIR MXNet_CPP_INCLUDE_DIR NNVM_INCLUDE_DIR DMLC_INCLUDE_DIR MXNet_LIBRARY MXNet_DIR)

set (MXNet_LIBRARIES ${MXNet_LIBRARY})
set (MXNet_INCLUDE_DIRS ${MXNet_C_INCLUDE_DIR} ${MXNet_CPP_INCLUDE_DIR}
                        ${NNVM_INCLUDE_DIR} ${DMLC_INCLUDE_DIR})

add_library (MXNet UNKNOWN IMPORTED)
set_target_properties (MXNet PROPERTIES IMPORTED_LOCATION ${MXNet_LIBRARY})
set_target_properties (MXNet PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${MXNet_INCLUDE_DIRS}")

if (MXNet_USE_STATIC_LIBS)
  set (MXNet_FIND_LIBRARY_SUFFIXES ${MXNet_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES})
endif ()