import os
import sys
import platform
from subprocess import run, PIPE, check_call

from setuptools import setup, Extension
from setuptools.command.build_ext import build_ext

assert (3, 7) <= sys.version_info < (4, 0), "Unsupported Python version"


class CMakeExtension(Extension):
    def __init__(self, target, name, *, package=None, sourcedir):
        if package is None:
            module = name
        else:
            module = package + "." + name
        super().__init__(module, sources=[])
        self.target = target
        self.sourcedir = os.path.abspath(sourcedir)


def use_egl():
    if platform.system() == "Windows":
        return False
    else:
        return True


class CMakeBuild(build_ext):
    def run(self):
        for ext in self.extensions:
            self.build_extension(ext)

    def build_extension(self, ext):
        env = os.environ.copy()
        extdir = os.path.abspath(os.path.dirname(self.get_ext_fullpath(ext.name)))
        cmake_args = [f"-DCMAKE_INSTALL_PREFIX={extdir}", f"-DPYTHON_EXECUTABLE={sys.executable}"]

        if "CMAKE_GENERATOR" in env:
            cmake_args += ["-G", env["CMAKE_GENERATOR"]]

        build_type = "Debug" if self.debug else "Release"
        cmake_args += [f"-DCMAKE_BUILD_TYPE={build_type}"]

        for var in [
            "CMAKE_PREFIX_PATH",
            "CMAKE_TOOLCHAIN_FILE",
            "VCPKG_TARGET_TRIPLET",
            "INSTALL_CUDA_COMPONENTS",
            "INSTALL_OPENCV_COMPONENTS",
            "INSTALL_SHARED_LIBRARIES",
        ]:
            if var in env:
                cmake_args += [f"-D{var}={env[var]}"]
        cmake_args += [
            "-DCMAKE_SKIP_INSTALL_ALL_DEPENDENCY=ON",
            "-DUSE_HDF5=ON",
            "-DUSE_MXNET=ON",
            "-DUSE_OPENGL=ON",
            "-DBUILD_PYTHON_EXTENSION=ON",
            "-DINSTALL_PYTHON_EXTENSION=ON",
        ]
        if use_egl():
            cmake_args += ["-DUSE_EGL=ON"]
        else:
            cmake_args += ["-DUSE_EGL=OFF"]

        build_dir = self.build_temp
        if not os.path.exists(build_dir):
            os.makedirs(build_dir)
        check_call(["cmake", ext.sourcedir] + cmake_args, cwd=build_dir, env=env)
        check_call(["cmake", "--build", ".", "--target", ext.target], cwd=build_dir, env=env)
        check_call(["cmake", "--build", ".", "--target", "install"], cwd=build_dir, env=env)


version_parts = (
    run(["git", "describe"], check=True, stdout=PIPE, encoding="utf-8").stdout.strip().split("-")
)
version = version_parts[0]
if len(version_parts) > 1:
    version += f"a{version_parts[1]}+{version_parts[2]}"

entry_points = {
    "console_scripts": [
        "robofish-birghan-ai-idtracker_to_trackset=robofish.birghan.ai.idtracker_to_trackset:main",
        "robofish-birghan-ai-simulator=robofish.birghan.ai.simulator:main",
        "robofish-birghan-ai-train=robofish.birghan.ai.ml.train:main",
        "robofish-birghan-ai-training_to_agent=robofish.birghan.ai.ml.training_to_agent:main",
    ],
    "gui_scripts": ["robofish-birghan-ai-trackset_viewer=robofish.birghan.ai.visualization.trackset_viewer:main"],
}

if use_egl():
    entry_points["console_scripts"].append(
        "robofish-birghan-ai-trackset_to_video=robofish.birghan.ai.visualization.trackset_to_video:main"
    )

setup(
    version=version,
    ext_modules=[CMakeExtension("python_extension", "cpp", package="robofish.birghan.ai", sourcedir="..")],
    entry_points=entry_points,
    cmdclass=dict(build_ext=CMakeBuild),
)
