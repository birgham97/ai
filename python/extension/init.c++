#include "python.h++"

#include <robofish/birghan_ai/agents/CouzinAgent.h++>
#include <robofish/birghan_ai/agents/MXNetAgent.h++>
#include <robofish/birghan_ai/common.h++>
#if HAS_OPENGL
#include <robofish/birghan_ai/opengl/SimpleFramebuffer.h++>
#include <robofish/birghan_ai/opengl/TracksetRenderer.h++>
#include <robofish/birghan_ai/opengl/ViewVectorsRenderer.h++>
#include <robofish/birghan_ai/opengl/FrameGrabber.h++>
#endif
#if HAS_EGL
#include <robofish/birghan_ai/opengl/HeadlessContext.h++>
#endif
#include <robofish/birghan_ai/ArrayBody.h++>
#include <robofish/birghan_ai/Agent.h++>
#include <robofish/birghan_ai/span.h++>

namespace robofish::ai::PyRandomState
{
	void bind(pybind11::module& m);
}
namespace robofish::ai::PyIBody
{
	void bind(pybind11::module& m);
}
namespace robofish::ai::PyArrayBody
{
	void bind(pybind11::module& m);
}
namespace robofish::ai::PyAgent
{
	void bind(pybind11::module& m);
}
namespace robofish::ai::PyCouzinAgent
{
	void bind(pybind11::module& m);
}
namespace robofish::ai::PyMXNetAgent
{
	void bind(pybind11::module& m);
}

PYBIND11_MODULE(cpp, cpp)
{
	using namespace pybind11;
	using namespace pybind11::literals;
	using namespace robofish::ai;
	using namespace robofish;

	PyRandomState::bind(cpp);

	PyIBody::bind(cpp);
	PyArrayBody::bind(cpp);
	PyAgent::bind(cpp);
	PyCouzinAgent::bind(cpp);
	PyMXNetAgent::bind(cpp);

	cpp.def(
	   "calc_locomotion",
	   [](array_t<float> poses_p_, array_t<float> poses_) {
		   if (poses_p_.ndim() == 1 && poses_.ndim() == 1) {
			   if (poses_p_.shape(0) == 4 && poses_.shape(0) == 4) {
				   auto poses_p = poses_p_.unchecked<1>();
				   auto poses   = poses_.unchecked<1>();
				   auto out_    = array_t<float>(std::vector<ssize_t>{2});
				   auto out     = out_.mutable_unchecked<1>();

				   std::array pose_p     = {poses_p(0), poses_p(1), poses_p(2), poses_p(3)};
				   std::array pose       = {poses(0), poses(1), poses(2), poses(3)};
				   auto       locomotion = calc_locomotion(pose_p, pose);
				   out(0)                = locomotion[0];
				   out(1)                = locomotion[1];
				   return out_;
			   }
		   } else if (poses_p_.ndim() == 2 && poses_.ndim() == 2) {
			   if (poses_p_.shape(0) == poses_.shape(0) && poses_p_.shape(1) == 4 && poses_.shape(1) == 4) {
				   auto poses_p = poses_p_.unchecked<2>();
				   auto poses   = poses_.unchecked<2>();
				   auto out_    = array_t<float>(std::vector<ssize_t>{poses_p_.shape(0), 2});
				   auto out     = out_.mutable_unchecked<2>();
				   for (ssize_t i = 0; i < poses_p.shape(0); ++i) {
					   std::array pose_p = {poses_p(i, 0), poses_p(i, 1), poses_p(i, 2), poses_p(i, 3)};
					   std::array pose   = {poses(i, 0), poses(i, 1), poses(i, 2), poses(i, 3)};

					   auto locomotion = calc_locomotion(pose_p, pose);
					   out(i, 0)       = locomotion[0];
					   out(i, 1)       = locomotion[1];
				   }
				   return out_;
			   }
		   }
		   throw std::invalid_argument("Arguments must have shape (4, ) or (_, 4)");
	   },
	   "poses_prev"_a,
	   "poses"_a);

	cpp.def("wall_distances",
	        vectorize([](interfaces::IBody& observer, stx::span<float> world, float ray_angle) {
		        return wall_distance(observer, {{0, 0}, {world[0], world[1]}}, ray_angle);
	        }),
	        "observer"_a,
	        "world"_a,
	        "rays"_a);

	cpp.def(
	   "centroid_distances",
	   [](interfaces::IBody&                              observer,
	      std::vector<std::shared_ptr<interfaces::IBody>> others,
	      stx::span<float>                                sector_limits) {
		   auto out = array_t<float>(sector_limits.size() - 1);
		   centroid_distances(stx::span<float>{out.mutable_data(), static_cast<std::size_t>(out.size())},
		                      observer,
		                      others,
		                      sector_limits);
		   return out;
	   },
	   "observer"_a,
	   "others"_a,
	   "sector_limits"_a);

	cpp.def("intensity_linear", vectorize(intensity_linear), "d"_a, "max_d"_a);
#if HAS_EGL
	class_<HeadlessContext>(cpp, "HeadlessContext").def(init<>()).def("make_current", &HeadlessContext::make_current);
#endif
#if HAS_OPENGL

	class_<TracksetRenderer>(cpp, "TracksetRenderer")
	   .def(init<float>(), "ppi"_a)
	   .def(
	      "draw",
	      [](TracksetRenderer&                          self,
	         std::tuple<GLint, GLint, GLsizei, GLsizei> viewport,
	         std::tuple<float, float>                   world,
	         std::vector<stx::span<float>>              agent_poses,
	         std::vector<std::string>                   agent_labels,
	         float                                      agent_radius,
	         float                                      agent_outline_thickness,
	         float                                      agent_tail_length) {
		      auto const [viewport_x, viewport_y, viewport_width, viewport_height] = viewport;
		      auto const [world_width, world_height]                               = world;
		      self.draw({viewport_x, viewport_y, viewport_width, viewport_height},
		                {world_width, world_height},
		                agent_poses,
		                agent_labels,
		                agent_radius,
		                agent_outline_thickness,
		                agent_tail_length);
	      },
	      "viewport"_a,
	      "world"_a,
	      "agent_poses"_a,
	      "agent_labels"_a,
	      "agent_radius"_a,
	      "agent_outline_thickness"_a,
	      "agent_tail_length"_a);

	class_<ViewVectorsRenderer>(cpp, "ViewVectorsRenderer")
	   .def(init<float>(), "ppi"_a)
	   .def(
	      "draw",
	      [](ViewVectorsRenderer&                       self,
	         std::tuple<GLint, GLint, GLsizei, GLsizei> viewport,
	         std::vector<stx::span<float>>              view_vectors,
	         float                                      outline_thickness,
	         std::tuple<GLfloat, GLfloat>               size,
	         std::tuple<GLfloat, GLfloat>               margins,
	         std::tuple<GLfloat, GLfloat>               spacing,
	         std::vector<std::string>                   labels) {
		      auto const [viewport_x, viewport_y, viewport_width, viewport_height] = viewport;
		      auto const [size_x, size_y]                                          = size;
		      auto const [margins_x, margins_y]                                    = margins;
		      auto const [spacing_x, spacing_y]                                    = spacing;
		      self.draw({viewport_x, viewport_y, viewport_width, viewport_height},
		                view_vectors,
		                outline_thickness,
		                {size_x, size_y},
		                {margins_x, margins_y},
		                {spacing_x, spacing_y},
		                labels);
	      },
	      "viewport"_a,
	      "view_vectors"_a,
	      "outline_thickness"_a,
	      "size"_a,
	      "margins"_a,
	      "spacing"_a,
	      "labels"_a);

	class_<SimpleFramebuffer>(cpp, "SimpleFramebuffer")
	   .def(init<GLsizei, GLsizei, int>(), "width"_a, "height"_a, "samples"_a)
	   .def("bind", &SimpleFramebuffer::bind);

	class_<FrameGrabber>(cpp, "FrameGrabber")
	   .def(init<int, int>(), "width"_a, "height"_a)
	   .def_property_readonly("frame_size", &FrameGrabber::frame_size)
	   .def(
	      "grab",
	      [](FrameGrabber& self, array_t<unsigned char> frame) {
		      auto frame_ = frame.mutable_unchecked<1>();
		      self.grab(&frame_[0]);
	      },
	      "frame"_a);

#endif
}
