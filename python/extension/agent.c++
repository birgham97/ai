#include <cstdint>
#include <memory>

#include <robofish/birghan_ai/agents/CouzinAgent.h++>
#include <robofish/birghan_ai/agents/MXNetAgent.h++>

#include "python.h++"

using namespace pybind11;
using namespace pybind11::literals;

using namespace robofish::interfaces;

namespace robofish::ai::PyIBody
{
	void bind(module& m)
	{
		class_<IBody, std::shared_ptr<IBody>>(m, "IBody");
	}
}

namespace robofish::ai::PyArrayBody
{
	void bind(module& m)
	{
		class_<ArrayBody, IBody, std::shared_ptr<ArrayBody>>(m, "ArrayBody")
		   .def(init<int, stx::span<float const, 2>, stx::span<float const, 2>>(), "uid"_a, "position"_a, "orientation"_a)
		   .def_readonly("uid", &ArrayBody::uid)
		   .def_property("pose",
		                 &ArrayBody::pose,
		                 [](ArrayBody& self, stx::span<float const, 4> pose) { self.set_pose(pose); })
		   .def("clone", &ArrayBody::clone);
	}
}

namespace robofish::ai::PyAgent
{
	class Trampoline : public Agent
	{
	public:
		using Agent::Agent;

		auto supported_time_steps() const -> std::vector<std::uint32_t> override
		{
			using return_type = std::vector<std::uint32_t>;
			PYBIND11_OVERLOAD(return_type, Agent, supported_time_steps, );
		}

		void reset() override
		{
			PYBIND11_OVERLOAD(void, Agent, reset, );
		}

		void tick(stx::span<std::shared_ptr<robofish::interfaces::IBody> const> bodies, std::uint32_t time_step) override
		{
			tick(std::vector<std::shared_ptr<robofish::interfaces::IBody>>{bodies.begin(), bodies.end()}, time_step);
		}

		void tick(std::vector<std::shared_ptr<robofish::interfaces::IBody>> const& bodies, std::uint32_t time_step)
		{
			PYBIND11_OVERLOAD_PURE(void, Agent, tick, bodies, time_step)
		}

		auto locomotion() const -> stx::span<float const> override
		{
			using return_type = stx::span<float const, 2>;
			PYBIND11_OVERLOAD_PURE(return_type, Agent, locomotion, )
		}
	};

	void bind(module& m)
	{
		class_<Agent, Trampoline, ArrayBody, std::shared_ptr<Agent>>(m, "Agent", dynamic_attr())
		   .def(init<stx::span<float const, 2>, int, stx::span<float const, 2>, stx::span<float const, 2>>(),
		        "world"_a,
		        "uid"_a,
		        "position"_a,
		        "orientation"_a)
		   .def_property_readonly("supported_time_steps", &Agent::supported_time_steps)
		   .def("reset", &Agent::reset)
		   .def(
		      "tick",
		      [](Agent& self, std::vector<std::shared_ptr<IBody>> const& bodies, std::uint32_t time_step) {
			      self.tick(bodies, time_step);
		      },
		      "bodies"_a,
		      "time_step"_a)
		   .def_property_readonly("locomotion", &Agent::locomotion);

		m.def("move_turn_speed", &move_turn_speed, "agent"_a, "locomotion"_a, "force_correct"_a = true);
	}
}

namespace robofish::ai::PyCouzinAgent
{
	void bind(module& m)
	{
		auto c = class_<CouzinAgent, Agent, std::shared_ptr<CouzinAgent>>(m, "CouzinAgent")
		            .def(init<stx::span<float const, 2>,
		                      int,
		                      stx::span<float const, 2>,
		                      stx::span<float const, 2>,
		                      CouzinAgent::Config>(),
		                 "world"_a,
		                 "uid"_a,
		                 "position"_a,
		                 "orientation"_a,
		                 "config"_a)
		            .def("seed", &CouzinAgent::seed, "seed"_a);

		class_<CouzinAgent::Config>(c, "Config")
		   .def(init<float, float, float, float, float, float, float, float>(),
		        "zor"_a,
		        "zoo"_a,
		        "zoa"_a,
		        "zob"_a,
		        "fop"_a,
		        "tr"_a,
		        "s"_a,
		        "sd"_a)
		   .def_static("load", &CouzinAgent::Config::load, "filename"_a);
	}
}

namespace robofish::ai::PyMXNetAgent
{
	void bind(module& m)
	{
		using NDArray = ::mxnet::cpp::NDArray;
		using Symbol  = ::mxnet::cpp::Symbol;

#if HAS_MXNET

		auto c = class_<MXNetAgent, Agent, std::shared_ptr<MXNetAgent>>(m, "MXNetAgent")
		            .def(init<stx::span<float const, 2>,
		                      int,
		                      stx::span<float const, 2>,
		                      stx::span<float const, 2>,
		                      MXNetAgent::Config>(),
		                 "world"_a,
		                 "uid"_a,
		                 "position"_a,
		                 "orientation"_a,
		                 "config"_a)
		            .def("seed", &MXNetAgent::seed, "seed"_a)
		            .def_property_readonly("time_step", &MXNetAgent::time_step)
		            .def_property("locomotion", &MXNetAgent::locomotion, &MXNetAgent::set_locomotion)
		            .def_property_readonly("arguments", &MXNetAgent::arguments)
		            .def_property_readonly("states", [](MXNetAgent const& self) {
			            std::map<std::string, NDArray> states;
			            for (decltype(auto) state : self.state_names()) {
				            states.insert(std::make_pair(state, self.arguments().at(state)));
			            }
			            return states;
		            });

		class_<MXNetAgent::Config>(c, "Config")
		   .def(init<Symbol,
		             std::map<std::string, NDArray>,
		             std::uint32_t,
		             std::array<std::tuple<float, float, int>, 2>,
		             std::optional<std::array<float, 2>>,
		             std::optional<std::array<float, 2>>,
		             float>(),
		        "symbols"_a,
		        "params"_a,
		        "time_step"_a,
		        "locomotion"_a,
		        "view_of_agents"_a,
		        "view_of_walls"_a,
		        "far_plane"_a)
#if HAS_HDF5
		   .def_static("load", &MXNetAgent::Config::load, "filename"_a)
#endif
		   .def_readonly("symbols", &MXNetAgent::Config::symbols)
		   .def_readonly("params", &MXNetAgent::Config::params)
		   .def_readonly("time_step", &MXNetAgent::Config::time_step)
		   .def_readonly("locomotion", &MXNetAgent::Config::locomotion)
		   .def_readonly("view_of_agents", &MXNetAgent::Config::view_of_agents)
		   .def_readonly("view_of_walls", &MXNetAgent::Config::view_of_walls)
		   .def_readonly("far_plane", &MXNetAgent::Config::far_plane)
		   .def("__getstate__",
		        [](MXNetAgent::Config const& self) {
			        return pybind11::make_tuple(self.symbols,
			                                    self.params,
			                                    self.time_step,
			                                    self.locomotion,
			                                    self.view_of_agents,
			                                    self.view_of_walls,
			                                    self.far_plane);
		        })
		   .def("__setstate__", [](MXNetAgent::Config& self, tuple t) {
			   if (t.size() != 8)
				   throw std::runtime_error("Invalid state!");

			   new (&self) MXNetAgent::Config{t[0].cast<Symbol>(),
			                                  t[1].cast<std::map<std::string, NDArray>>(),
			                                  t[2].cast<std::uint32_t>(),
			                                  t[3].cast<std::array<std::tuple<float, float, int>, 2>>(),
			                                  t[4].cast<std::optional<std::array<float, 2>>>(),
			                                  t[5].cast<std::optional<std::array<float, 2>>>(),
			                                  t[6].cast<float>()};
		   });
#endif
	}
}
