
pybind11_add_module(python_extension python.h++ random.c++ agent.c++ init.c++)
set_target_properties(python_extension PROPERTIES OUTPUT_NAME cpp)

target_link_libraries(python_extension PRIVATE lib)

if(INSTALL_PYTHON_EXTENSION)
	install(TARGETS python_extension OPTIONAL DESTINATION .)
endif()

set_target_properties(python_extension PROPERTIES INSTALL_RPATH "\$ORIGIN;\$ORIGIN/../../mxnet;")
