import sys

assert (3, 7) <= sys.version_info < (4, 0), "Unsupported Python version"

# Ensure the shared libraries for Qt5 are loaded as the C++ extension links against them
import PySide2.QtCore
import PySide2.QtGui
import PySide2.QtWidgets
import mxnet  # Ensure the shared library for MXNet is loaded as the C++ extension links against it
import robofish.birghan.ai.cpp  # Load C++ extension
