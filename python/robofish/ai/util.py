from os import urandom
from sys import byteorder
from datetime import datetime
from typing import Tuple, Iterable
from numpy import int64
from numpy.random import RandomState


def timestamp(when: datetime = None) -> str:
    when = when or datetime.utcnow()
    return f"{when:%Y%m%dT%H%M%SU%f}"


def randseed(random: RandomState = None) -> int:
    if random is not None:
        return int(random.randint(0, 2 ** 32, dtype=int64))
    else:
        return int.from_bytes(urandom(4), byteorder)
