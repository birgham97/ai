import sys
from ast import literal_eval
from math import ceil
from argparse import ArgumentParser, Action
from pathlib import Path

from numpy import float32, pi, sin, cos, empty

import h5py

from robofish.birghan.ai.util import randseed, timestamp
from robofish.birghan.ai.cpp import RandomState, CouzinAgent, MXNetAgent


def uniform_pose(world, margins, random):
    width, height = world
    hmargin, vmargin = margins
    ori = random.uniform(0, 2 * pi)
    return (
        float32(
            [random.uniform(hmargin, width - hmargin), random.uniform(vmargin, height - vmargin)]
        ),
        float32([cos(ori), sin(ori)]),
    )


def to_num_frames(time, time_step) -> int:
    amount, unit = time
    if unit == "frames":
        return int(amount)
    elif unit == "seconds":
        return int(ceil(amount * 1000.0 / time_step))
    else:
        assert False


def select_time_step(agents, default_time_step):
    time_steps = {}
    num_specialized_agents = 0
    for agent in agents:
        supported = agent.supported_time_steps
        if supported:
            num_specialized_agents += 1
        for ts in supported:
            time_steps[ts] = time_steps.get(ts, 0) + 1
    if num_specialized_agents > 0:
        # Get the lowest time step supported by all time specialized agents
        time_steps = sorted(time_steps.items(), key=lambda item: item[1], reverse=True)
        time_steps = list(filter(lambda item: item[1] == time_steps[0][1], time_steps))
        time_steps = sorted(time_steps, key=lambda item: item[0], reverse=False)
        assert num_specialized_agents == time_steps[0][1]
        return time_steps[0][0]
    else:
        return default_time_step


def encode_agent_type(agent):
    if isinstance(agent, CouzinAgent):
        return "couzin"
    elif isinstance(agent, MXNetAgent):
        return "mxnet"
    else:
        assert False


def run_with_agents(agents, runtime, trackset, default_time_step=40):

    time_step = select_time_step(agents, default_time_step)

    num_frames = to_num_frames(runtime, time_step)

    trackset_is_hdf5 = isinstance(trackset, h5py.Group)

    if trackset_is_hdf5:
        trackset.attrs["num_frames"] = num_frames
        trackset.attrs["time_step"] = time_step

    tracks = {}
    for agent in agents:
        if trackset_is_hdf5:
            track = trackset.create_dataset(
                f"{agent.uid}", shape=(num_frames, 4), dtype=float32, chunks=True
            )
            track.attrs["agent_type"] = encode_agent_type(agent)
            if hasattr(agent, "config_file"):
                track.attrs["config_file"] = agent.config_file
        else:
            track = empty(shape=(num_frames, 4), dtype=float32)
            trackset[f"{agent.uid}"] = track
        tracks[agent] = track
        track[0] = agent.pose

    for t in range(1, num_frames):
        bodies = [agent.clone() for agent in agents]
        for agent, track in tracks.items():
            agent.tick(bodies, time_step)
            track[t] = agent.pose

    return num_frames, time_step


def run(
    world,
    agent_configs,
    margins,
    runtime,
    *,
    seed=None,
    initial_pose=uniform_pose,
    default_time_step=40,
    tags=[],
    trackset=None,
):
    if isinstance(trackset, h5py.Group):
        pass
    elif trackset is None:
        trackset = {}
    else:
        trackset = h5py.File(str(trackset), "w")
    trackset_is_hdf5 = isinstance(trackset, h5py.Group)

    seed = seed if seed is not None else randseed()
    random = RandomState(seed)

    agents = []
    uid = 0
    for agent_type, amount, config_file in agent_configs:
        for _ in range(amount):
            config = agent_type.Config.load(str(config_file.resolve(strict=True)))
            agent = agent_type(world, uid, *initial_pose(world, margins, random), config)
            uid += 1
            if hasattr(agent, "seed"):
                agent.seed(random)
            agent.config_file = str(config_file)
            agents.append(agent)

    run_with_agents(agents, runtime, trackset, default_time_step)

    if trackset_is_hdf5:
        trackset.attrs["seed"] = seed

        trackset.attrs["world"] = world
        trackset.attrs["margins"] = margins
        if tags:
            trackset.attrs["tags"] = tags

    return trackset


def main(args=None):
    def parse_args(args):
        class AgentAction(Action):
            def __init__(self, option_strings, dest, nargs=None, **kwargs):
                assert nargs is None
                super().__init__(
                    option_strings, dest, 2, metavar=("AMOUNT", "CONFIG_FILE"), **kwargs
                )

            def __call__(self, parser, namespace, values, option_string=None):
                items = getattr(namespace, self.dest, None)
                if items is None:
                    items = []
                items.append((int(values[0]), Path(values[1])))
                setattr(namespace, self.dest, items)

        p = ArgumentParser()

        p.add_argument("--tags", default=[], nargs="+", help="Tags attached to the trackset")

        o = p.add_mutually_exclusive_group()
        o.add_argument(
            "--output-directory",
            "--od",
            default=Path("."),
            type=Path,
            help="Directory in which to store an automatically named trackset file",
        )
        o.add_argument(
            "-o", "--output-file", type=Path, help="Path to a manually named trackset file"
        )

        p.add_argument(
            "--world",
            default=float32([100, 100]),
            type=lambda s: float32([*literal_eval(s)]),
            help="width,height of the world in centimeters",
        )

        margins = p.add_mutually_exclusive_group()
        margins.add_argument(
            "--margin",
            default=None,
            type=float,
            help="Spawn agents MARGIN centimeters away from the world's boundaries",
        )
        margins.add_argument(
            "--margins",
            default=float32([20, 20]),
            type=float32,
            help="Spawn agents HMARGIN/VMARGIN centimeters away from the world's horizontal/vertical boundaries",
            nargs=2,
            metavar=("HMARGIN", "VMARGIN"),
        )

        p.add_argument(
            "--couzin",
            default=[],
            action=AgentAction,
            help="Spawn AMOUNT couzin agents with parameters from CONFIG_FILE",
        )
        p.add_argument(
            "--mxnet",
            default=[],
            action=AgentAction,
            help="Spawn AMOUNT mxnet agents with parameters from CONFIG_FILE",
        )

        p.add_argument("--seed", default=None, type=int, help="Root seed for PRNG")

        p.add_argument(
            "-ts",
            "--default-time-step",
            default=40,
            type=int,
            help="Default amount of milliseconds between two frames",
        )

        time = p.add_mutually_exclusive_group(required=True)
        time.add_argument(
            "--num-frames", default=None, type=int, help="Number of frames to simulate"
        )
        time.add_argument(
            "--num-seconds", default=None, type=float, help="Minimum number of seconds to simulate"
        )

        args = p.parse_args(args)

        if args.margin is not None:
            args.margins = [args.margin, args.margin]
            del args.margin

        return args

    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)

    if args.output_file:
        trackset_file = args.output_file
    else:
        trackset_file = (args.output_directory / "_".join(args.tags + [timestamp()])).with_suffix(
            ".hdf5"
        )

    agents = []
    agents += [(CouzinAgent, amount, config_file) for amount, config_file in args.couzin]
    agents += [(MXNetAgent, amount, config_file) for amount, config_file in args.mxnet]

    amount = args.num_frames or args.num_seconds
    if args.num_frames is not None:
        unit = "frames"
    elif args.num_seconds is not None:
        unit = "seconds"
    else:
        assert False
    run(
        args.world,
        agents,
        args.margins,
        (amount, unit),
        seed=args.seed,
        default_time_step=args.default_time_step,
        tags=args.tags,
        trackset=trackset_file,
    )
