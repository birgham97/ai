__all__ = ["Split", "Activation"]

from mxnet.gluon import HybridBlock


class Split(HybridBlock):
    def __init__(self, output_sizes, prefix=None, params=None):
        super().__init__(prefix, params)
        self._output_sizes = output_sizes

        def indices():
            begin = 0
            for size in output_sizes:
                end = begin + size
                yield begin, end
                begin = end

        self.output_indices = tuple(indices())

    def hybrid_forward(self, F, x):
        return tuple(x.slice_axis(-1, begin, end) for begin, end in self.output_indices)

    def __repr__(self):
        return f"{self.__class__.__name__}({sum(self._output_sizes)} -> {self._output_sizes})"


class Activation(HybridBlock):
    def __init__(self, activation, prefix=None, params=None):
        self._activation = activation
        super().__init__(prefix, params)

    def hybrid_forward(self, F, *inputs):
        if self._activation == "softmax":
            activation = F.softmax
        else:
            activation = lambda x: F.Activation(x, act_type=self._activation)
        if type(inputs) == tuple or type(inputs) == list:
            return tuple(activation(x) for x in inputs)
        else:
            return activation(inputs)

    def __repr__(self):
        return f"{self.__class__.__name__}({self._activation})"
