from mxnet import ndarray
from mxnet.gluon import ParameterDict, Block


class Injector(type):
    def __new__(cls, clsname, superclasses, attributedict, *, target):
        for attr in (
            attr
            for attr in attributedict.items()
            if attr[0] != "__module__" and attr[0] != "__qualname__"
        ):
            setattr(target, attr[0], attr[1])

        return type.__new__(cls, clsname, superclasses, attributedict)

    def __init__(cls, name, bases, attr, **kwargs):
        type.__init__(cls, name, bases, attr)


class _ParameterDict(metaclass=Injector, target=ParameterDict):
    def export_dict(self, strip_prefix=""):
        """Converts a ParameterDict into a dict.
        strip_prefix : str, default ''
            Strip prefix from parameter names.
        """
        arg_dict = {}
        for param in self.values():
            weight = param._reduce()
            if not param.name.startswith(strip_prefix):
                raise ValueError(
                    f"Prefix {strip_prefix} is to be striped before saving, but Parameter "
                    "{param.name} does not start with {strip_prefix}. If you are using Block.save_params, "
                    "This may be due to your Block shares parameters from other "
                    "Blocks or you forgot to use ``with name_scope()`` during init. "
                    "Consider switching to Block.collect_params.save and "
                    "Block.collect_params.load instead."
                )
            arg_dict[param.name[len(strip_prefix) :]] = weight
        return arg_dict

    def import_dict(
        self, arg_dict, ctx, allow_missing=False, ignore_extra=False, restore_prefix=""
    ):
        """Import parameters from a dictionary.
        ctx : Context or list of Context
            Context(s) initialize loaded parameters on.
        allow_missing : bool, default False
            Whether to silently skip loading parameters not represents in the file.
        ignore_extra : bool, default False
            Whether to silently ignore parameters from the file that are not
            present in this ParameterDict.
        restore_prefix : str, default ''
            prepend prefix to names of stored parameters before loading.
        """
        if restore_prefix:
            for name in self.keys():
                assert name.startswith(restore_prefix), (
                    f"restore_prefix is {restore_prefix} but Parameters name {restore_prefix} does not start "
                    "with {restore_prefix}"
                )
        lprefix = len(restore_prefix)
        loaded = [
            (k[4:] if k.startswith("arg:") or k.startswith("aux:") else k, v)
            for k, v in arg_dict.items()
        ]
        arg_dict = {restore_prefix + k: v for k, v in loaded}
        if not allow_missing:
            for name in self.keys():
                assert name in arg_dict, f"Parameter {name[lprefix:]} is missing"
        for name in arg_dict:
            if name not in self._params:
                assert ignore_extra, f"Parameter {name[lprefix:]} is not present in ParameterDict"
                continue

            self[name]._load_init(arg_dict[name], ctx)


class _Block(metaclass=Injector, target=Block):
    def save_params_hdf5(self, group, strip_prefix=True):
        """Save parameters in an HDF5 group.

        group : h5py._hl.group.Group
            HDF5 group
        strip_prefix : bool, default True
            Strip the block's prefix from the parameter names."""
        for k, v in (
            self.collect_params()
            .export_dict(strip_prefix=(self.prefix if strip_prefix else ""))
            .items()
        ):
            group[k] = v.asnumpy()

    def load_params_hdf5(
        self, group, ctx, allow_missing=False, ignore_extra=False, restore_prefix=True
    ):
        """Load parameters from an HDF5 group

        group : h5py._hl.group.Group
            HDF5 group
        ctx : Context or list of Context
            Context(s) initialize loaded parameters on.
        allow_missing : bool, default False
            Whether to silently skip loading parameters not represents in the file.
        ignore_extra : bool, default False
            Whether to silently ignore parameters from the file that are not
            present in this Block.
        restore_prefix : bool, default True
            Prepend the block's prefix to names of stored parameters before loading."""
        self.collect_params().import_dict(
            {k: ndarray.array(v, ctx) for k, v in group.items()},
            ctx,
            allow_missing,
            ignore_extra,
            (self.prefix if restore_prefix else ""),
        )
