__all__ = ["Unroller", "RecurrentSequential"]

from mxnet import ndarray, symbol
from mxnet.gluon import HybridBlock


def _as_list(obj):
    if isinstance(obj, (list, tuple)):
        return obj
    else:
        return [obj]


def _format_sequence(length, inputs, merge):
    if isinstance(inputs, symbol.Symbol):
        if not merge:
            assert len(inputs.list_outputs()) == 1
            inputs = list(inputs.split(axis=0, num_outputs=length, squeeze_axis=1))
    elif isinstance(inputs, ndarray.NDArray):
        if not merge:
            assert length == inputs.shape[0]
            inputs = _as_list(inputs.split(axis=0, num_outputs=length, squeeze_axis=1))
    else:
        assert len(inputs) == length
        if isinstance(inputs[0], symbol.Symbol):
            F = symbol
        else:
            F = ndarray
        if merge:
            inputs = F.stack(*inputs, axis=0)

    return inputs


def unroll(block, length, inputs, begin_state):
    assert length >= 1, "Cannot unroll into less than one timestep"
    block.reset()

    inputs = _format_sequence(length, inputs, False)

    output, state = block(inputs[0], begin_state)
    multi_output = type(output) == tuple or type(output) == list
    if not multi_output:
        outputs = [output]

        for t in range(1, length):
            output, state = block(inputs[t], state)
            outputs.append(output)

        outputs = _format_sequence(length, outputs, True)
        return outputs, state
    else:
        outputs = [[_] for _ in output]

        for t in range(1, length):
            output, state = block(inputs[t], state)
            for i in range(len(output)):
                outputs[i].append(output[i])

        _outputs = []
        for output in outputs:
            output = _format_sequence(length, output, True)
            _outputs.append(output)
        return tuple(_outputs), state


class Unroller(HybridBlock):
    def __init__(self, length, prefix=None, params=None):
        super().__init__(prefix=prefix, params=params)
        self.length = length

    def add(self, *blocks):
        for block in blocks:
            self.register_child(block)

    def hybrid_forward(self, F, *args, **kwargs):
        out = tuple(
            unroll(block, self.length, *args, **kwargs) for block in self._children.values()
        )
        if len(out) == 1:
            out = out[0]
        return out

    def __repr__(self):
        return f"{self.__class__.__name__}({{','.join(repr(block) for block in self._children.values())}})"


class RecurrentSequential(HybridBlock):
    def __init__(self, prefix=None, params=None):
        super().__init__(prefix=prefix, params=params)

    def add(self, *blocks):
        for block in blocks:
            self.register_child(block)

    @property
    def layers(self):
        return list(self._children.values())

    def hybrid_forward(self, F, x, states):
        next_states = []
        for layer, layer_states in zip(self.layers, states):
            multi_input = type(x) == tuple or type(x) == list
            if not multi_input:
                if layer_states:
                    x, layer_next_states = layer(x, layer_states)
                else:
                    x, layer_next_states = layer(x), ()
            else:
                if layer_states:
                    x, layer_next_states = layer(*x, layer_states)
                else:
                    x, layer_next_states = layer(*x), ()
            next_states.append(layer_next_states)
        return x, next_states

    def state_info(self, batch_size=0):
        return [
            layer.state_info(batch_size=batch_size)
            if callable(getattr(layer, "state_info", None))
            else ()
            for layer in self.layers
        ]

    def begin_state(self, batch_size=0, func=ndarray.zeros, **kwargs):
        return [
            layer.begin_state(batch_size=batch_size, func=func, **kwargs)
            if hasattr(layer, "begin_state")
            else ()
            for layer in self.layers
        ]

    def reset(self):
        for layer in self.layers:
            if callable(getattr(layer, "reset", None)):
                layer.reset()

    def __repr__(self):
        return f"{self.__class__.__name__}({','.join(repr(layer) for layer in self.layers)})"
