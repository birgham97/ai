from mxnet import symbol
from mxnet.gluon import HybridBlock


# Generate Symbols for the (recurrent) states of a list of layers
def symbol_states(layers, i=0):
    states = []
    for layer in layers:
        layer_states = []
        for _ in layer.state_info():
            layer_states.append(symbol.var(f"state{i}"))
            i += 1
        states.append(layer_states)
    return states, i
