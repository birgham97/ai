import sys

from argparse import ArgumentParser
from pathlib import Path

import h5py


def export(training, epoch, output_directory=None, output_file=None):
    if not isinstance(training, h5py.Group):
        training = h5py.File(str(training), "r")

    if output_file:
        agent = output_file
    else:
        training_filename = Path(training.file.filename).resolve()
        agent = (output_directory or training_filename.parent) / (
            "agent_" + training_filename.stem + ".hdf5"
        )

    if not isinstance(agent, h5py.Group):
        agent = h5py.File(str(agent), "w")

    epoch = training["params"][sorted(training["params"].keys())[epoch]]

    agent.attrs["training"] = training.file.filename
    agent.attrs["symbols"] = training.attrs["symbols"]
    agent.attrs["num_hidden"] = training.attrs["num_hidden"]
    agent.attrs["time_step"] = training.attrs["time_step"]
    agent.attrs["far_plane"] = training.attrs["far_plane"]
    agent.attrs["locomotion"] = training.attrs["locomotion"]
    if "view_of_agents" in training.attrs:
        agent.attrs["view_of_agents"] = training.attrs["view_of_agents"]
    if "view_of_walls" in training.attrs:
        agent.attrs["view_of_walls"] = training.attrs["view_of_walls"]
    for name, dataset in epoch.items():
        agent[name] = dataset[:]
    if "tags" in training.attrs:
        agent.attrs["tags"] = training.attrs["tags"]


def main(args=None):
    def parse_args(args):
        p = ArgumentParser(description="Convert model from training file to agent configuration.")
        p.add_argument("training", type=Path, help="Path to training file to convert model from")
        p.add_argument(
            "--epoch", default=-1, type=int, help="Epoch from which to use the parameters"
        )

        o = p.add_mutually_exclusive_group()
        o.add_argument(
            "--output-directory",
            "--od",
            type=Path,
            help="Directory in which to store an automatically named agent configuration file",
        )
        o.add_argument(
            "-o",
            "--output-file",
            type=Path,
            help="Path to a manually named agent configuration file",
        )

        args = p.parse_args(args)

        return args

    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    export(**args.__dict__)
