from argparse import Action
from math import floor, log10

from numpy import zeros, float32

from robofish.birghan.ai.ml.train import prepare_view_bins, raycast_agent_centroids_and_walls
from robofish.birghan.ai.cpp import TracksetRenderer, ViewVectorsRenderer


def extract_view_vectors(
    world, trackset, num_frames, ids, far_plane, view_of_agents, view_of_walls
):
    if view_of_walls == "match" and view_of_agents:
        fop, n = view_of_agents
        view_of_walls = fop - fop / n, n
    view_of_agents, view_of_walls = prepare_view_bins(view_of_agents, view_of_walls)
    view_of_agents_size, _ = view_of_agents
    view_of_walls_size, _ = view_of_walls

    view_vectors = {
        id: zeros((num_frames, view_of_agents_size + view_of_walls_size), dtype=float32)
        for id in ids
    }
    raycast_agent_centroids_and_walls(
        world, trackset, num_frames, view_of_agents, view_of_walls, far_plane, view_vectors, 0
    )
    return view_vectors


class RenderConfig:
    def __init__(
        self,
        draw_labels,
        draw_view_vectors,
        *,
        far_plane=142.0,
        view_of_agents=None,
        view_of_walls=None,
    ):
        self.draw_labels = draw_labels
        assert (
            not draw_view_vectors or view_of_agents is not None or view_of_walls is not None
        ), "view_of_agents or view_of_walls required"
        self.draw_view_vectors = draw_view_vectors
        self.far_plane = far_plane
        self.view_of_agents = view_of_agents
        self.view_of_walls = view_of_walls


class Renderer:
    def __init__(self, config):
        self.world = None
        self.trackset = None
        self.num_frames = None
        self.trackset_renderer = None
        self.view_vectors = None
        self.view_vectors_renderer = None
        self.config = config

    def set_ppi(self, value):
        if self.trackset_renderer is not None:
            del self.trackset_renderer
        if self.view_vectors_renderer is not None:
            del self.view_vectors_renderer
        self.trackset_renderer = TracksetRenderer(value)
        self.view_vectors_renderer = ViewVectorsRenderer(value)

    def trackset_viewport(self, screen_width, screen_height):
        if self.trackset is None:
            return None
        aspect_ratio = self.world[0] / self.world[1]
        height = screen_height
        width = height * aspect_ratio
        if width > screen_width:
            height *= screen_width / width
            width = screen_width
        return 0, screen_height - int(height), int(width), int(height)

    def view_vectors_viewport(self, screen_width, screen_height):
        trackset_viewport = self.trackset_viewport(screen_width, screen_height)
        free_width, free_height = (
            screen_width - trackset_viewport[2],
            screen_height - trackset_viewport[3],
        )
        if free_width > free_height:
            return (
                trackset_viewport[0] + trackset_viewport[2],
                trackset_viewport[1],
                free_width,
                trackset_viewport[3],
            )
        elif free_width < free_height:
            return (
                trackset_viewport[0],
                trackset_viewport[1] - free_height,
                trackset_viewport[2],
                free_height,
            )
        else:
            return None

    def draw(self, width, height, frame):
        if self.trackset is None:
            return
        self.trackset_renderer.draw(
            self.trackset_viewport(width, height),
            self.world,
            list(self.trackset[id][frame] for id in self.ids),
            self.labels,
            self.agent_radius,
            0.05,
            0.5,
        )
        if self.view_vectors:
            view_vectors_viewport = self.view_vectors_viewport(width, height)
            if view_vectors_viewport is not None:
                self.view_vectors_renderer.draw(
                    view_vectors_viewport,
                    list(self.view_vectors[id][frame] for id in self.ids),
                    0.05,
                    (3.0, 0.75),
                    (0.5, 0.5),
                    (0.5, 0.5),
                    self.labels,
                )

    def set_data(self, world, trackset, num_frames):
        self.world = tuple(world)
        self.trackset = trackset
        self.num_frames = num_frames
        self.ids = list(trackset.keys())
        self.labels = [] if not self.config.draw_labels else [str(id) for id in self.ids]

        if self.config.draw_view_vectors:
            self.view_vectors = extract_view_vectors(
                self.world,
                self.trackset,
                self.num_frames,
                self.ids,
                self.config.far_plane,
                self.config.view_of_agents,
                self.config.view_of_walls,
            )

        if self.config.draw_labels:
            max_label_len = 1 + floor(log10(len(self.ids)))
            self.agent_radius = 0.125 * max_label_len
        else:
            self.agent_radius = 0.1

    def __del__(self):
        if self.trackset_renderer:
            del self.trackset_renderer
        if self.view_vectors_renderer:
            del self.view_vectors_renderer


class ViewAction(Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        assert nargs is None
        super().__init__(
            option_strings, dest, 2, metavar=("field of perception", "number of bins"), **kwargs
        )

    def __call__(self, parser, namespace, values, option_string=None):
        items = (float32(values[0]), int(values[1]))
        setattr(namespace, self.dest, items)
