
#include "MXNetBehavior.h++"

#include <robofish/birghan_ai/common.h++>
#include <robofish/birghan_ai/hdf5.h++>
#include <robofish/birghan_ai/mxnet.h++>

namespace robofish::ai
{
	void MXNetBehavior::setConfig(QString const& filename)
	{
		try {
			AgentBehavior::setConfig(filename);
		} catch (H5::Exception const& e) {
			qWarning() << "Failed to load agent configuration from" << filename;
		} catch (dmlc::Error const& e) {
			qWarning() << MXGetLastError();
			qWarning() << "Failed to load agent configuration from" << filename;
		} catch (std::exception const& e) {
			qWarning() << e.what();
			qWarning() << "Failed to load agent configuration from" << filename;
		}
	}

	auto MXNetBehavior::supportedTimesteps() -> std::vector<std::uint32_t>
	{
		return {config->time_step};
	}

	void MXNetBehavior::activate(SharedBody robot, interfaces::IModelWorldDescriptor* world)
	{
		try {
			AgentBehavior::activate(robot, world);
		} catch (H5::Exception const& e) {
			qWarning() << "Failed to instantiate agent";
		} catch (dmlc::Error const& e) {
			qWarning() << MXGetLastError();
			qWarning() << "Failed to instantiate agent";
		} catch (std::exception const& e) {
			qWarning() << e.what();
			qWarning() << "Failed to instantiate agent";
		}
	}
}
