
#include <QtGlobal>

#include <robofish/interfaces/IBehaviorFactory.h>

#include "MXNetBehavior.h++"

namespace robofish::ai
{
	class Q_DECL_EXPORT BehaviorFactory : public interfaces::IBehaviorFactory
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "de.fu-berlin.mi.biorobotics.robofish.IBehaviorFactory" FILE "plugin.json")
		Q_INTERFACES(robofish::interfaces::IBehaviorFactory)
	public:
		std::shared_ptr<interfaces::IBehavior> makeHeadless(QString const& configFile) override
		{
			return std::make_shared<MXNetBehavior>(configFile);
		}

		std::shared_ptr<interfaces::IBehavior> makeInteractive(QGridLayout* layout, QGraphicsView* scene) override
		{
			return std::make_shared<MXNetBehavior>(layout, scene);
		}
	};
}
#include "BehaviorFactory.moc"
