
#include <robofish/birghan_ai/agents/CouzinAgent.h++>
#include <robofish/birghan_ai/robotracker/AgentBehavior.h++>

namespace robofish::ai
{
	class Q_DECL_EXPORT CouzinBehavior : public AgentBehavior<CouzinAgent>
	{
		Q_OBJECT
		Q_INTERFACES(robofish::interfaces::IBehavior)

	public:
		using AgentBehavior::AgentBehavior;

		auto supportedTimesteps() -> std::vector<std::uint32_t> override;

		void activate(SharedBody robot, interfaces::IModelWorldDescriptor* world) override;

		void setConfig(QString const& filename) override;
	};
}
