#pragma once

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include <robofish/birghan_ai/opengl/Program.h++>
#include <robofish/birghan_ai/opengl/Buffer.h++>
#include <robofish/birghan_ai/opengl/VertexArray.h++>

namespace robofish::ai
{
	class RectangleRenderer
	{
		RectangleRenderer(RectangleRenderer const& src) = delete;
		RectangleRenderer(RectangleRenderer&& src)      = delete;
		RectangleRenderer& operator=(RectangleRenderer const& rhs) = delete;
		RectangleRenderer& operator=(RectangleRenderer&& rhs) = delete;

		Program     program_m{Shader{":/robofish-birghan-ai/shaders/default/vertex.glsl", GL_VERTEX_SHADER},
                        Shader{":/robofish-birghan-ai/shaders/default/fragment.glsl", GL_FRAGMENT_SHADER}};
		VertexArray vao_m;
		Buffer      vbo_m;

	public:
		RectangleRenderer();

		void draw(glm::vec2 position, glm::vec2 size, float thickness, glm::vec4 color, glm::mat4 const& world_to_clip);
	};
}
