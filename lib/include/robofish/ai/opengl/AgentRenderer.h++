#pragma once

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include <robofish/birghan_ai/opengl/Program.h++>
#include <robofish/birghan_ai/opengl/VertexArray.h++>
#include <robofish/birghan_ai/opengl/Buffer.h++>
#include <robofish/birghan_ai/opengl/TextRenderer.h++>

namespace robofish::ai
{
	class AgentRenderer
	{
		AgentRenderer() = delete;

		AgentRenderer(AgentRenderer const& src) = delete;
		AgentRenderer(AgentRenderer&& src)      = delete;
		AgentRenderer& operator=(AgentRenderer const& rhs) = delete;
		AgentRenderer& operator=(AgentRenderer&& rhs) = delete;

		const int sides_m;

		Program     program_m{Shader{":/robofish-birghan-ai/shaders/default/vertex.glsl", GL_VERTEX_SHADER},
                        Shader{":/robofish-birghan-ai/shaders/default/fragment.glsl", GL_FRAGMENT_SHADER}};
		VertexArray vao_m;
		Buffer      vbo_m;

		int num_vertices() const;

	public:
		AgentRenderer(int sides);
		void draw(glm::vec4        pose,
		          float            radius,
		          float            outline_thickness,
		          float            tail_length,
		          glm::vec3        color,
		          glm::mat4 const& world_to_clip);
		void draw_with_label(glm::vec4        pose,
		                     std::string      label,
		                     float            label_min_radius,
		                     float            outline_thickness,
		                     float            tail_length,
		                     glm::vec3        color,
		                     glm::mat4 const& world_to_clip,
		                     TextRenderer&    text_renderer,
		                     float            px_to_world);
	};
}
