#pragma once

#include <stdexcept>
#include <type_traits>
#include <initializer_list>
#include <functional>

#include <epoxy/gl.h>

#include <robofish/birghan_ai/Resource.h++>
#include <robofish/birghan_ai/opengl/Shader.h++>

namespace robofish::ai
{
	class Program : public Resource<GLuint, Program>
	{
		friend class Resource<GLuint, Program>;

		static Handle acquire()
		{
			return glCreateProgram();
		}

		static void release(Handle handle)
		{
			glDeleteProgram(handle);
		}

	public:
		Program(Shader const& shader)
		: Program(std::initializer_list<std::reference_wrapper<const Shader>>{std::cref(shader)})
		{
		}

		template<
		   typename... Shaders /*, std::enable_if_t<(std::is_convertible_v<Shaders, Shader const&> && ...), int> = 0*/>
		Program(Shaders&&... shaders)
		: Program(std::initializer_list<std::reference_wrapper<const Shader>>{std::cref(shaders)...})
		{
		}

		template<typename Shaders>
		Program(Shaders&& shaders)
		: Resource()
		{
			for (decltype(auto) shader : shaders) {
				glAttachShader(handle_m, [](Shader const& shader) { return shader.handle(); }(shader));
			}
			glLinkProgram(handle_m);
			for (decltype(auto) shader : shaders) {
				glDetachShader(handle_m, [](Shader const& shader) { return shader.handle(); }(shader));
			}

			GLint is_linked = 0;
			glGetProgramiv(handle_m, GL_LINK_STATUS, &is_linked);
			if (!is_linked) {
				GLint log_len = 0;
				glGetProgramiv(handle_m, GL_INFO_LOG_LENGTH, &log_len);

				std::vector<GLchar> log(log_len);
				glGetProgramInfoLog(handle_m, log_len, &log_len, &log[0]);

				glDeleteProgram(handle_m);

				throw std::runtime_error(&log[0]);
			}
		}
	};
}
