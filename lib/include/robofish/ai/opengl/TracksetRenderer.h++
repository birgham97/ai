#pragma once

#include <robofish/birghan_ai/opengl/TextRenderer.h++>
#include <robofish/birghan_ai/opengl/RectangleRenderer.h++>
#include <robofish/birghan_ai/opengl/AgentRenderer.h++>

#include <glm/vec4.hpp>
#include <glm/vec2.hpp>

namespace robofish::ai
{
	class TracksetRenderer
	{
		TracksetRenderer() = delete;

		TracksetRenderer(TracksetRenderer const& src) = delete;
		TracksetRenderer(TracksetRenderer&& src)      = delete;
		TracksetRenderer& operator=(TracksetRenderer const& rhs) = delete;
		TracksetRenderer& operator=(TracksetRenderer&& rhs) = delete;

		float ppi_m;

		TextRenderer      text_renderer_m;
		RectangleRenderer rectangle_renderer_m;
		AgentRenderer     agent_renderer_m{32};

		void clear();

	public:
		TracksetRenderer(float ppi);

		void draw(glm::uvec4                  viewport,
		          glm::vec2                   world,
		          stx::span<stx::span<float>> agent_poses,
		          stx::span<std::string>      agent_labels,
		          float                       agent_radius,
		          float                       agent_outline_thickness,
		          float                       agent_tail_length);
	};
}
