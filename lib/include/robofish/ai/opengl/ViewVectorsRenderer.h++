#pragma once

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

#include <robofish/birghan_ai/opengl/QuadRenderer.h++>
#include <robofish/birghan_ai/opengl/RectangleRenderer.h++>
#include <robofish/birghan_ai/opengl/TextRenderer.h++>

namespace robofish::ai
{
	class ViewVectorsRenderer
	{
		float             ppcm_m;
		QuadRenderer      quad_renderer_m;
		RectangleRenderer rectangle_renderer_m;
		TextRenderer      text_renderer_m;

	public:
		ViewVectorsRenderer(float ppi);

		void draw(glm::uvec4                  viewport,
		          stx::span<stx::span<float>> view_vectors,
		          float                       outline_thickness,
		          glm::vec2                   size,
		          glm::vec2                   margins,
		          glm::vec2                   spacing,
		          stx::span<std::string>      labels);
	};
}
