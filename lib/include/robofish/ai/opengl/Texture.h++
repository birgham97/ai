#pragma once

#include <epoxy/gl.h>

#include <robofish/birghan_ai/Resource.h++>

namespace robofish::ai
{
	class Texture : public Resource<GLuint, Texture>
	{
		friend class Resource<GLuint, Texture>;
		using Resource::Resource;

		static Handle acquire(GLenum target)
		{
			Handle h;
			glCreateTextures(target, 1, &h);
			return h;
		}

		static void release(Handle handle)
		{
			glDeleteTextures(1, &handle);
		}
	};
}
