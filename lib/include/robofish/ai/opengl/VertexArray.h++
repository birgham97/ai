#pragma once

#include <epoxy/gl.h>

#include <robofish/birghan_ai/Resource.h++>

namespace robofish::ai
{
	class VertexArray : public Resource<GLuint, VertexArray>
	{
		friend class Resource<GLuint, VertexArray>;
		using Resource::Resource;

		static Handle acquire()
		{
			Handle h;
			glCreateVertexArrays(1, &h);
			return h;
		}

		static void release(Handle handle)
		{
			glDeleteVertexArrays(1, &handle);
		}
	};
}
