#pragma once

#include <system_error>
#include <vector>
#include <iostream>
#include <string>

#include <robofish/birghan_ai/common.h++>

#include <QString>
#include <QFile>

#include <epoxy/gl.h>

#include <robofish/birghan_ai/Resource.h++>

namespace robofish::ai
{
	class Shader : public Resource<GLuint, Shader>
	{
		friend class Resource<GLuint, Shader>;

		static Handle acquire(GLenum type)
		{
			return glCreateShader(type);
		}

		static void release(Handle handle)
		{
			glDeleteShader(handle);
		}

	public:
		Shader(std::vector<char> const& src, GLenum type)
		: Resource(type)
		{
			GLchar const* src_list = &src[0];
			GLint         len_list = src.size();

			glShaderSource(handle_m, 1, &src_list, &len_list);

			glCompileShader(handle_m);
			GLint is_compiled = false;
			glGetShaderiv(handle_m, GL_COMPILE_STATUS, &is_compiled);

			if (!is_compiled) {
				GLint log_len = 0;
				glGetShaderiv(handle_m, GL_INFO_LOG_LENGTH, &log_len);

				std::vector<GLchar> log(log_len);
				glGetShaderInfoLog(handle_m, log_len, &log_len, &log[0]);

				glDeleteShader(handle_m);

				throw std::runtime_error(&log[0]);
			}
		}

		Shader(std::string filename, GLenum type)
		: Shader(read_all(QFile(QString(filename.c_str()))), type)
		{
		}
	};
}
