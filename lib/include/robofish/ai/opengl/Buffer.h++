#pragma once

#include <epoxy/gl.h>

#include <robofish/birghan_ai/Resource.h++>

namespace robofish::ai
{
	class Buffer : public Resource<GLuint, Buffer>
	{
		friend class Resource<GLuint, Buffer>;
		using Resource::Resource;

		static Handle acquire()
		{
			Handle h;
			glCreateBuffers(1, &h);
			return h;
		}

		static void release(Handle handle)
		{
			glDeleteBuffers(1, &handle);
		}
	};
}
