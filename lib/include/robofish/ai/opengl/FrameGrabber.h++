#pragma once

#include <robofish/birghan_ai/opengl/SimpleFramebuffer.h++>

namespace robofish::ai
{
	class FrameGrabber
	{
		int       width_m;
		int       height_m;
		int const num_channels_m{4};
		int const bytes_per_channel_m{1};
		int const frame_size_m{width_m * height_m * num_channels_m * bytes_per_channel_m};

		SimpleFramebuffer framebuffer_m;

	public:
		int frame_size() const;

		FrameGrabber(int width, int height);
		void grab(unsigned char* frame);
	};
}
