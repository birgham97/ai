#pragma once

#include <vector>
#include <map>
#include <string>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <hb.h>
#include <hb-ft.h>

#include <robofish/birghan_ai/Resource.h++>

#include <robofish/birghan_ai/opengl/Texture.h++>

#include <robofish/birghan_ai/opengl/Shader.h++>
#include <robofish/birghan_ai/opengl/Program.h++>

#include <robofish/birghan_ai/opengl/Buffer.h++>
#include <robofish/birghan_ai/opengl/VertexArray.h++>

namespace robofish::ai
{
	enum struct HAlignment { Left, Center, Right };

	enum struct VAlignment { Top, Center, Bottom };

	class TextRenderer
	{
		Program shader_program_m;

		Buffer      glyph_vbo_m;
		VertexArray glyph_vao_m;

		class FreeType_Library : public Resource<FT_Library, FreeType_Library>
		{
			friend class Resource<FT_Library, FreeType_Library>;
			using Resource::Resource;

			static Handle acquire()
			{
				FT_Library handle;
				if (FT_Init_FreeType(&handle)) {
					throw std::runtime_error("Could not initialize FreeType library");
				}
				return handle;
			}

			static void release(Handle handle)
			{
				FT_Done_FreeType(handle);
			}
		};

		class FreeType_Face : public Resource<FT_Face, FreeType_Face>
		{
			friend class Resource<FT_Face, FreeType_Face>;
			using Resource::Resource;

			static Handle acquire(FreeType_Library const& library,
			                      FT_Byte const*          file_base,
			                      FT_Long                 file_size,
			                      FT_Long                 face_index)
			{
				FT_Face face;
				if (FT_New_Memory_Face(library, file_base, file_size, face_index, &face)) {
					throw std::runtime_error("Failed to load FreeType face");
				}
				return face;
			}

			static void release(Handle handle)
			{
				FT_Done_Face(handle);
			}
		};

		FreeType_Library  ft_library_m;
		std::vector<char> ft_font_data_m;
		FreeType_Face     ft_face_m;

		class Glyph : public Texture
		{
			friend class TextRenderer;

			glm::ivec2 size;
			glm::ivec2 bearing;

		public:
			Glyph(FreeType_Face const& face, char32_t codepoint);
		};

		std::map<char32_t, Glyph> rendered_glyphs_m;

		TextRenderer(std::string font_filename, FT_Long font_face);

	public:
		TextRenderer(std::string font_filename, FT_Long font_face, FT_UInt font_height, float ppi);

		auto draw(std::string const&                 text,
		          glm::vec3                          color,
		          std::tuple<HAlignment, VAlignment> alignment,
		          glm::mat4 const&                   mvp_matrix) -> std::tuple<float, float, float, float>;
	};
}
