#pragma once

#include <array>

#include <epoxy/gl.h>

#include <robofish/birghan_ai/opengl/Framebuffer.h++>
#include <robofish/birghan_ai/opengl/Renderbuffer.h++>

namespace robofish::ai
{
	class SimpleFramebuffer : public Framebuffer
	{
		std::array<Renderbuffer, 2> renderbuffers_m;

	public:
		SimpleFramebuffer(GLsizei width, GLsizei height, int samples)
		: Framebuffer()
		{
			if (samples > 1) {
				glNamedRenderbufferStorageMultisample(renderbuffers_m[0], samples, GL_RGBA8, width, height);
				glNamedRenderbufferStorageMultisample(renderbuffers_m[1], samples, GL_DEPTH_COMPONENT16, width, height);
			} else {
				glNamedRenderbufferStorage(renderbuffers_m[0], GL_RGBA8, width, height);
				glNamedRenderbufferStorage(renderbuffers_m[1], GL_DEPTH_COMPONENT16, width, height);
			}

			glNamedFramebufferRenderbuffer(*this, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, renderbuffers_m[0]);
			glNamedFramebufferRenderbuffer(*this, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderbuffers_m[1]);

			if (glCheckNamedFramebufferStatus(*this, GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
				throw std::runtime_error("OpenGL Error: Failed to setup framebuffer, try requesting "
				                         "fewer samples");
			}

			glNamedFramebufferDrawBuffer(*this, GL_COLOR_ATTACHMENT0);
		}

		void bind()
		{
			glBindFramebuffer(GL_FRAMEBUFFER, *this);
		}
	};
}
