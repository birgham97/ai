#pragma once

#include <epoxy/gl.h>

#include <robofish/birghan_ai/Resource.h++>

namespace robofish::ai
{
	class Framebuffer : public Resource<GLuint, Framebuffer>
	{
		friend class Resource<GLuint, Framebuffer>;
		using Resource::Resource;

		static Handle acquire()
		{
			Handle h;
			glCreateFramebuffers(1, &h);
			return h;
		}

		static void release(Handle handle)
		{
			glDeleteFramebuffers(1, &handle);
		}
	};
}
