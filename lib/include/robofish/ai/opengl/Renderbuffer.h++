#pragma once

#include <epoxy/gl.h>

#include <robofish/birghan_ai/Resource.h++>

namespace robofish::ai
{
	class Renderbuffer : public Resource<GLuint, Renderbuffer>
	{
		friend class Resource<GLuint, Renderbuffer>;
		using Resource::Resource;

		static Handle acquire()
		{
			Handle h;
			glCreateRenderbuffers(1, &h);
			return h;
		}

		static void release(Handle handle)
		{
			glDeleteRenderbuffers(1, &handle);
		}
	};
}
