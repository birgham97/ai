#pragma once

#include <string>

#include <robofish/birghan_ai/Agent.h++>
#include <robofish/birghan_ai/RandomState.h++>

namespace robofish::ai
{
	class CouzinAgent : public Agent
	{
	protected:
		RandomState random;

		float zor, zoo, zoa, zob;
		float rr, ro, ra;
		float fop;
		float tr, s;
		float sd;

		std::array<float, 2> locomotion_m;

	public:
		struct Config
		{
			float zor;
			float zoo;
			float zoa;
			float zob;
			float fop;
			float tr;
			float s;
			float sd;

			static auto           load(std::string const& filename) -> Config;
			static constexpr auto filename_extension = "xml";
		};

		CouzinAgent(stx::span<float const, 2> world,
		            int                       uid,
		            stx::span<float const, 2> position,
		            stx::span<float const, 2> orientation,
		            Config                    config);

		void tick(stx::span<std::shared_ptr<interfaces::IBody> const> bodies, std::uint32_t time_step) override;
		auto locomotion() const -> stx::span<float const> override;

		void seed(RandomState& seed);
	};
}
