#pragma once

namespace robofish::ai
{
	template<typename H, typename T>
	class Resource
	{
	protected:
		using Handle = H;
		Handle handle_m;

		Resource(Resource const& src) = delete;
		Resource(Resource&& src)      = delete;
		Resource& operator=(Resource const& rhs) = delete;
		Resource& operator=(Resource&& rhs) = delete;

	public:
		template<typename... Args>
		Resource(Args&&... args)
		: handle_m(T::acquire(std::forward<Args>(args)...))
		{
		}

		virtual ~Resource()
		{
			T::release(handle_m);
		}

		Handle handle() const
		{
			return handle_m;
		}
		operator Handle() const
		{
			return handle_m;
		}
		Handle operator->() const
		{
			return handle_m;
		}
	};
}
