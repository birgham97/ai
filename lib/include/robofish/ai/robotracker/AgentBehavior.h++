#pragma once

#include <memory>
#include <optional>

#include <QtGlobal>
#include <QDebug>

#include <QFormLayout>
#include <QComboBox>

#include <robofish/birghan_ai/qt/FileSelect.h++>
#include <robofish/birghan_ai/robotracker/behavior.h++>

#include <robofish/birghan_ai/robotracker/AgentBehaviorBase.h++>

namespace robofish::ai
{
	template<typename Agent>
	class AgentBehavior : public AgentBehaviorBase
	{
	protected:
		std::optional<typename Agent::Config> config;
		std::optional<Agent>                  agent;
		interfaces::IModelWorldDescriptor*    world;
		std::shared_ptr<interfaces::IBody>    prev_body;
		QGridLayout*                          layout;

	public:
		AgentBehavior(QString const& configFile)
		{
			setConfig(configFile);
		}

		AgentBehavior(QGridLayout* layout, QGraphicsView* scene)
		: world(nullptr)
		, layout(layout)
		{
			auto vbox = new QVBoxLayout;
			vbox->setSpacing(0);

			auto settings = new QFormLayout();
			settings->setSpacing(0);

			auto config = new FileSelect(nullptr,
			                             "Agent configuration",
			                             ".",
			                             "Configuration files (*." + QString(Agent::Config::filename_extension) + ")");
			QObject::connect(config, &FileSelect::fileSelected, this, &AgentBehaviorBase::setConfig);
			settings->addRow("Config", config);

			vbox->addLayout(settings);

			vbox->addStretch();

			layout->addLayout(vbox, 0, 0, -1, -1);
		}

		~AgentBehavior()
		{
			if (layout)
				removeAll(layout);
			this->agent = std::nullopt;
			deactivate();
		}

		RobotActionList nextSpeeds(BodyList robots, BodyList fish, uint32_t timeStep) override
		{
			auto [body, others] = collect(prev_body->uid, robots, fish);
			if (!body) {
				qWarning() << "nextSpeeds called without assigned robot having been detected";
				return {};
			}

			if (!agent) {
				deactivate();
				return {std::make_shared<RobotActionHalt>(body->uid, 0)};
			}

			auto prev_body  = this->prev_body;
			this->prev_body = body;

			// Set (previously predicted) configuration to actually observed one
			agent->set_pose(body->position().x, body->position().y, body->orientation()[0], body->orientation()[1]);
			// Infer next configuration
			agent->tick(others, timeStep);

			return {SharedRobotAction{
			   new RobotActionTurningForward{body->uid, 0, agent->locomotion()[0], agent->locomotion()[1]}}};
		}

		void activate(SharedBody robot, interfaces::IModelWorldDescriptor* world) override
		{
			if (!config)
				return;

			agent.emplace(
			   std::array<float, 2>{static_cast<float>(world->getWidth()), static_cast<float>(world->getHeight())},
			   robot->uid,
			   std::array<float, 2>{robot->position().x, robot->position().y},
			   std::array<float, 2>{robot->orientation()[0], robot->orientation()[1]},
			   *config);
			qDebug() << "Agent instantiated";

			this->prev_body = robot;
			this->world     = world;
			IBehavior::activate(robot, world);
		}

		void deactivate() override
		{
			this->world = nullptr;
			IBehavior::deactivate();
		}

		void setConfig(QString const& filename) override
		{
			agent  = std::nullopt;
			config = Agent::Config::load(filename.toStdString());
			qDebug() << "Agent configuration loaded from" << filename;
		}
	};
}
