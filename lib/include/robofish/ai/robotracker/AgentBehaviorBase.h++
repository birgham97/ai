#pragma once

#include <robofish/interfaces/IBehavior.h>

namespace robofish::ai
{
	class AgentBehaviorBase : public interfaces::IBehavior
	{
		Q_OBJECT

	public:
		virtual void setConfig(QString const& filename) = 0;
	};
}
