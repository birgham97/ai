#pragma once

#include <array>

#include <robofish/interfaces/IBody.h>

#include <robofish/birghan_ai/span.h++>

namespace robofish::ai
{
	class ArrayBody : public interfaces::IBody
	{
	protected:
		int                  uid_m;
		std::array<float, 4> pose_m;

	public:
		ArrayBody(int uid, stx::span<float const, 2> position, stx::span<float const, 2> orientation);
		ArrayBody(interfaces::IBody const& other);
		ArrayBody(ArrayBody const& other);
		ArrayBody& operator=(ArrayBody const& other) = delete;

		auto pose() const -> stx::span<float const, 4>;

		void set_pose(stx::span<float const, 4> pose);
		void set_pose(float x, float y, float dx, float dy);
		void set_position(stx::span<float const, 2> position);
		void set_orientation(stx::span<float const, 2> orientation);

		auto position() const -> cv::Point2f override;
		auto orientation() const -> cv::Vec2f override;

		virtual auto clone() const -> ArrayBody;
	};
}
