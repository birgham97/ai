#version 450 core

uniform mat4 model_to_clip;

in vec4 position;

void main()
{
    gl_Position = model_to_clip * position;
}
