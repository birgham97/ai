#version 450 core

uniform mat4 mvp_matrix;

in vec4 vertex; // <vec2 pos, vec2 tex>
out vec2 TexCoords;


void main()
{
    gl_Position = mvp_matrix * vec4(vertex.xy, 0.0, 1.0);
    TexCoords = vertex.zw;
}
