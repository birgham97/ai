#include <robofish/birghan_ai/opengl/TextRenderer.h++>

#include <epoxy/gl.h>

#include <robofish/birghan_ai/common.h++>

namespace robofish::ai
{
	TextRenderer::Glyph::Glyph(FreeType_Face const& face, char32_t codepoint)
	: Texture(GL_TEXTURE_2D)
	{
		if (FT_Load_Glyph(face, codepoint, FT_LOAD_RENDER)) {
			std::cerr << "Failed to load glyph for codepoint U+" << std::setfill('0') << std::setw(4) << std::hex
			          << codepoint << std::endl;
			return;
		}

		GLint unpack_alignment;
		glGetIntegerv(GL_UNPACK_ALIGNMENT, &unpack_alignment);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		glTextureStorage2D(*this, 1, GL_R8, face->glyph->bitmap.width, face->glyph->bitmap.rows);
		glTextureSubImage2D(*this,
		                    0,
		                    0,
		                    0,
		                    face->glyph->bitmap.width,
		                    face->glyph->bitmap.rows,
		                    GL_RED,
		                    GL_UNSIGNED_BYTE,
		                    face->glyph->bitmap.buffer);

		glTextureParameteri(*this, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTextureParameteri(*this, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTextureParameteri(*this, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTextureParameteri(*this, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glPixelStorei(GL_UNPACK_ALIGNMENT, unpack_alignment);

		size    = {face->glyph->bitmap.width, face->glyph->bitmap.rows};
		bearing = {face->glyph->bitmap_left, face->glyph->bitmap_top};
	}

	TextRenderer::TextRenderer(std::string font_filename, FT_Long font_face)
	: shader_program_m(Shader{":/robofish-birghan-ai/shaders/text/vertex.glsl", GL_VERTEX_SHADER},
	                   Shader{":/robofish-birghan-ai/shaders/text/fragment.glsl", GL_FRAGMENT_SHADER})
	, ft_font_data_m(read_all(QFile(QString(font_filename.c_str()))))
	, ft_face_m(ft_library_m, reinterpret_cast<FT_Byte const*>(&ft_font_data_m[0]), ft_font_data_m.size(), font_face)
	{
		auto const binding = 0;
		glVertexArrayVertexBuffer(glyph_vao_m, binding, glyph_vbo_m, 0, 4 * sizeof(float));

		auto const vertex_attribute = glGetAttribLocation(shader_program_m, "vertex");
		glEnableVertexArrayAttrib(glyph_vao_m, vertex_attribute);
		glVertexArrayAttribFormat(glyph_vao_m, vertex_attribute, 4, GL_FLOAT, GL_FALSE, 0);
		glVertexArrayAttribBinding(glyph_vao_m, vertex_attribute, binding);

		// NOTE: One quad consists of two triangles with three four-dimensional
		// float vertices
		glNamedBufferStorage(glyph_vbo_m, 2 * 3 * 4 * sizeof(float), NULL, GL_DYNAMIC_STORAGE_BIT);
	}

	TextRenderer::TextRenderer(std::string font_filename, FT_Long font_face, FT_UInt font_height, float ppi)
	: TextRenderer(font_filename, font_face)
	{
		// NOTE: Freetype wants height in 1/64 fractions
		FT_Set_Char_Size(ft_face_m, 0, font_height * 64, ppi, ppi);
	}

	auto TextRenderer::draw(std::string const&                 text,
	                        glm::vec3                          color,
	                        std::tuple<HAlignment, VAlignment> alignment,
	                        glm::mat4 const&                   mvp_matrix) -> std::tuple<float, float, float, float>
	{
		// Determine bounding box around text
		auto font = hb_ft_font_create(ft_face_m.handle(), NULL);

		auto buffer = hb_buffer_create();
		hb_buffer_add_utf8(buffer, &text[0], text.size(), 0, text.size());

		hb_buffer_guess_segment_properties(buffer);

		hb_shape(font, buffer, NULL, 0);

		auto glyph_count     = hb_buffer_get_length(buffer);
		auto glyph_info      = hb_buffer_get_glyph_infos(buffer, NULL);
		auto glyph_positions = hb_buffer_get_glyph_positions(buffer, NULL);

		auto min_x = 0.f;
		auto min_y = 0.f;
		auto max_x = 0.f;
		auto max_y = 0.f;
		{
			auto cursor_x = 0.f;
			auto cursor_y = 0.f;
			for (decltype(glyph_count) i = 0; i < glyph_count; ++i) {
				auto const codepoint = glyph_info[i].codepoint;

				rendered_glyphs_m.try_emplace(codepoint, ft_face_m, codepoint);
				decltype(auto) glyph = rendered_glyphs_m.at(codepoint);

				auto const x_offset  = glyph_positions[i].x_offset / 64.0f;
				auto const y_offset  = glyph_positions[i].y_offset / 64.0f;
				auto const x_advance = glyph_positions[i].x_advance / 64.0f;
				auto const y_advance = glyph_positions[i].y_advance / 64.0f;

				auto const xpos = cursor_x + x_offset + glyph.bearing.x;
				auto const ypos = cursor_y + y_offset - (glyph.size.y - glyph.bearing.y);

				auto const w = glyph.size.x;
				auto const h = glyph.size.y;

				min_x = std::min(min_x, xpos);
				min_y = std::min(min_y, ypos);
				max_x = std::max(max_x, xpos + w);
				max_y = std::max(max_y, ypos + h);

				cursor_x += x_advance;
				cursor_y += y_advance;
			}
		}

		auto cursor_x = [&]() {
			auto halignment = std::get<0>(alignment);
			switch (halignment) {
			case HAlignment::Left:
				return 0.f;
			case HAlignment::Right:
				return -(max_x - min_x);
			case HAlignment::Center:
				return -(max_x - min_x) / 2;
			default:
				return 0.f;
			}
		}();
		auto cursor_y = [&]() {
			auto valignment = std::get<1>(alignment);
			switch (valignment) {
			case VAlignment::Bottom:
				return 0.f;
			case VAlignment::Top:
				return -(max_y - min_y);
			case VAlignment::Center:
				return -(max_y - min_y) / 2;
			default:
				return 0.f;
			}
		}();
		min_x += cursor_x;
		max_x += cursor_x;
		min_y += cursor_y;
		max_y += cursor_y;

		// Draw text in a 1:1 glyph <-> texture mapping

		glUseProgram(shader_program_m);
		glUniformMatrix4fv(glGetUniformLocation(shader_program_m, "mvp_matrix"), 1, GL_FALSE, &mvp_matrix[0][0]);

		glUniform3f(glGetUniformLocation(shader_program_m, "text_color"), color.x, color.y, color.z);
		glBindVertexArray(glyph_vao_m);

		for (decltype(glyph_count) i = 0; i < glyph_count; ++i) {
			auto codepoint = glyph_info[i].codepoint;

			rendered_glyphs_m.try_emplace(codepoint, ft_face_m, codepoint);
			decltype(auto) glyph = rendered_glyphs_m.at(codepoint);

			auto const x_offset  = glyph_positions[i].x_offset / 64.0f;
			auto const y_offset  = glyph_positions[i].y_offset / 64.0f;
			auto const x_advance = glyph_positions[i].x_advance / 64.0f;
			auto const y_advance = glyph_positions[i].y_advance / 64.0f;

			auto const xpos = cursor_x + x_offset + glyph.bearing.x;
			auto const ypos = cursor_y + y_offset - (glyph.size.y - glyph.bearing.y);

			auto const w = glyph.size.x;
			auto const h = glyph.size.y;

			GLfloat vertices[6][4] = {{xpos, ypos + h, 0.0, 0.0},
			                          {xpos, ypos, 0.0, 1.0},
			                          {xpos + w, ypos, 1.0, 1.0},

			                          {xpos, ypos + h, 0.0, 0.0},
			                          {xpos + w, ypos, 1.0, 1.0},
			                          {xpos + w, ypos + h, 1.0, 0.0}};

			glBindTextureUnit(0, glyph);

			glNamedBufferSubData(glyph_vbo_m, 0, sizeof(vertices), vertices);

			glDrawArrays(GL_TRIANGLES, 0, 6);

			cursor_x += x_advance;
			cursor_y += y_advance;
		}

		hb_buffer_destroy(buffer);
		hb_font_destroy(font);

		glBindTextureUnit(0, 0);

		return {min_x, min_y, max_x, max_y};
	}
}
