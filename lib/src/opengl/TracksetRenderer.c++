#include <robofish/birghan_ai/opengl/TracksetRenderer.h++>

#include <epoxy/gl.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

namespace robofish::ai
{
	TracksetRenderer::TracksetRenderer(float ppi)
	: ppi_m{ppi}
	, text_renderer_m(":/robofish-birghan-ai/fonts/IBMPlexMono-Regular.otf", 0, 12, ppi)
	{
	}

	void TracksetRenderer::clear()
	{
		glClearColor(1, 1, 1, 1);
		glClearDepth(1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void TracksetRenderer::draw(glm::uvec4                  viewport,
	                            glm::vec2                   world,
	                            stx::span<stx::span<float>> agent_poses,
	                            stx::span<std::string>      agent_labels,
	                            float                       agent_radius,
	                            float                       agent_outline_thickness,
	                            float                       agent_tail_length)
	{
		glEnable(GL_SCISSOR_TEST);

		glDisable(GL_DEPTH_TEST);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glViewport(viewport.x, viewport.y, viewport.z, viewport.w);
		glScissor(viewport.x, viewport.y, viewport.z, viewport.w);

		clear();

		glDisable(GL_SCISSOR_TEST);

		auto const world_to_clip = glm::scale(glm::vec3{1, -1, 1}) * glm::ortho(0.f, world.x, 0.f, world.y, 0.f, 2.f)
		                           * glm::lookAt(glm::vec3{0, 0, 1}, glm::vec3{0, 0, 0}, glm::vec3{0, 1, 0});

		auto const ppcm = ppi_m / 2.54f;

		auto const px_to_world = (world.x / viewport.z + world.y / viewport.w) / 2.f;
		auto const cm_to_world = px_to_world * ppcm;

		rectangle_renderer_m.draw({0, 0}, {world.x, world.y}, 0.1f * cm_to_world, {0, 0, 0, 1}, world_to_clip);

		for (std::size_t i = 0; i < agent_poses.size(); ++i) {
			decltype(auto) pose = agent_poses[i];
			if (i < agent_labels.size()) {
				decltype(auto) label = agent_labels[i];
				agent_renderer_m.draw_with_label({pose[0], pose[1], pose[2], pose[3]},
				                                 label,
				                                 agent_radius * cm_to_world,
				                                 agent_outline_thickness * cm_to_world,
				                                 agent_tail_length * cm_to_world,
				                                 {0, 0, 0},
				                                 world_to_clip,
				                                 text_renderer_m,
				                                 px_to_world);
			} else {
				agent_renderer_m.draw({pose[0], pose[1], pose[2], pose[3]},
				                      agent_radius * cm_to_world,
				                      agent_outline_thickness * cm_to_world,
				                      agent_tail_length * cm_to_world,
				                      {0, 0, 0},
				                      world_to_clip);
			}
		}
	}
}
