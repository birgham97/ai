#include <robofish/birghan_ai/opengl/QuadRenderer.h++>

#include <array>

#include <epoxy/gl.h>

namespace robofish::ai
{
	constexpr static std::size_t num_vertices = 4;

	QuadRenderer::QuadRenderer()
	{
		auto const binding = 0;
		glVertexArrayVertexBuffer(vao_m, binding, vbo_m, 0, 4 * sizeof(float));

		auto const position_attribute = glGetAttribLocation(program_m, "position");
		glEnableVertexArrayAttrib(vao_m, position_attribute);
		glVertexArrayAttribFormat(vao_m, position_attribute, 4, GL_FLOAT, GL_FALSE, 0);
		glVertexArrayAttribBinding(vao_m, position_attribute, binding);

		glNamedBufferStorage(vbo_m, num_vertices * sizeof(glm::vec4), NULL, GL_DYNAMIC_STORAGE_BIT);
	}

	void QuadRenderer::draw(glm::vec2 position, glm::vec2 size, glm::vec4 color, glm::mat4 const& world_to_clip)
	{
		glm::vec4 const tl{position.x, position.y, 0, 1};
		glm::vec4 const tr{position.x + size.x, position.y, 0, 1};
		glm::vec4 const bl{position.x, position.y + size.y, 0, 1};
		glm::vec4 const br{position.x + size.x, position.y + size.y, 0, 1};

		std::array vertices = {tl, bl, tr, br};

		glNamedBufferSubData(vbo_m, 0, num_vertices * sizeof(glm::vec4), &vertices[0]);

		glUseProgram(program_m);
		glBindVertexArray(vao_m);
		glUniformMatrix4fv(glGetUniformLocation(program_m, "model_to_clip"), 1, GL_FALSE, &world_to_clip[0][0]);
		glUniform4fv(glGetUniformLocation(program_m, "color"), 1, &color[0]);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, num_vertices);
	}
}
