#include <robofish/birghan_ai/opengl/AgentRenderer.h++>

#include <vector>

#include <epoxy/gl.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>

#include <robofish/birghan_ai/common.h++>

namespace robofish::ai
{
	int AgentRenderer::num_vertices() const
	{
		return (2 + 2 * sides_m + 4 + 2);
	}

	AgentRenderer::AgentRenderer(int sides)
	: sides_m(sides)
	{
		auto const binding = 0;
		glVertexArrayVertexBuffer(vao_m, binding, vbo_m, 0, 4 * sizeof(float));

		auto const position_attribute = glGetAttribLocation(program_m, "position");
		glEnableVertexArrayAttrib(vao_m, position_attribute);
		glVertexArrayAttribFormat(vao_m, position_attribute, 4, GL_FLOAT, GL_FALSE, 0);
		glVertexArrayAttribBinding(vao_m, position_attribute, binding);

		glNamedBufferStorage(vbo_m, num_vertices() * sizeof(glm::vec4), NULL, GL_DYNAMIC_STORAGE_BIT);
	}

	void AgentRenderer::draw(glm::vec4        pose,
	                         float            radius,
	                         float            outline_thickness,
	                         float            tail_length,
	                         glm::vec3        color,
	                         glm::mat4 const& world_to_clip)
	{
		radius += outline_thickness;
		auto const origin_to_clip = world_to_clip * glm::translate(glm::vec3{pose[0], pose[1], 0});

		auto const offset = outline_thickness / 2.f;

		std::vector<glm::vec4> vertices(num_vertices());

		// Regular convex polygon around label
		vertices[0] = {radius - offset, 0, 0, 1};
		vertices[1] = {radius + offset, 0, 0, 1};

		for (auto i = 1; i <= sides_m; ++i) {
			auto const angle = i * 2 * std::acos(-1) / sides_m;

			vertices[i * 2] = {std::cos(angle) * (radius - offset), -std::sin(angle) * (radius - offset), 0, 1};

			vertices[i * 2 + 1] = {std::cos(angle) * (radius + offset), -std::sin(angle) * (radius + offset), 0, 1};
		}

		// Tail
		auto const orient = glm::rotate(angle2D(glm::vec2{1, 0}, glm::vec2{pose.z, pose.w}), glm::vec3{0, 0, 1});

		vertices[num_vertices() - 4] = orient * glm::vec4{-radius, -offset, 0, 1};
		vertices[num_vertices() - 3] = orient * glm::vec4{-radius - tail_length, -offset, 0, 1};
		vertices[num_vertices() - 2] = orient * glm::vec4{-radius, offset, 0, 1};
		vertices[num_vertices() - 1] = orient * glm::vec4{-radius - tail_length, offset, 0, 1};

		// Insert degenerate triangles
		vertices[num_vertices() - 5] = vertices[num_vertices() - 4];
		vertices[num_vertices() - 6] = vertices[num_vertices() - 7];

		glNamedBufferSubData(vbo_m, 0, num_vertices() * sizeof(glm::vec4), &vertices[0]);

		glUseProgram(program_m);

		auto const model_to_clip = origin_to_clip;

		glBindVertexArray(vao_m);
		glUniformMatrix4fv(glGetUniformLocation(program_m, "model_to_clip"), 1, GL_FALSE, &model_to_clip[0][0]);
		glUniform4f(glGetUniformLocation(program_m, "color"), color.r, color.g, color.b, 1);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, num_vertices());
	}

	void AgentRenderer::draw_with_label(glm::vec4        pose,
	                                    std::string      label,
	                                    float            label_min_radius,
	                                    float            outline_thickness,
	                                    float            tail_length,
	                                    glm::vec3        color,
	                                    glm::mat4 const& world_to_clip,
	                                    TextRenderer&    text_renderer,
	                                    float            px_to_world)
	{
		auto const origin_to_clip = world_to_clip * glm::translate(glm::vec3{pose[0], pose[1], 0});

		auto const radius = std::max(label_min_radius, [&]() {
			auto [min_x, min_y, max_x, max_y] = text_renderer.draw(
			   label,
			   color,
			   {HAlignment::Center, VAlignment::Center},
			   origin_to_clip * glm::scale(glm::vec3{1, -1, 1}) * glm::scale(glm::vec3{px_to_world, px_to_world, 1}));

			auto dx = (max_x - min_x) / 2.f;
			auto dy = (max_y - min_y) / 2.f;

			return std::sqrt(dx * dx + dy * dy);
		}() * px_to_world);

		draw(pose, radius, outline_thickness, tail_length, color, world_to_clip);
	}
}
