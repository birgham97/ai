#include <robofish/birghan_ai/common.h++>

#include <system_error>

#include <CGAL/intersections.h>

#include <QFile>

#include <robofish/interfaces/IBody.h>

struct Qt5Resources
{
	Qt5Resources()
	{
		Q_INIT_RESOURCE(robofish_ai);
	}

	~Qt5Resources()
	{
		Q_CLEANUP_RESOURCE(robofish_ai);
	}
};

namespace robofish::ai
{
	auto operator+(Vector const& a, cv::Vec2f const& b) -> Vector
	{
		return {a[0] + b[0], a[1] + b[1]};
	}

	auto contains(Rectangle const& r, Point const& p) -> bool
	{
		return r.xmin() <= p.x() && p.x() <= r.xmax() && r.ymin() <= p.y() && p.y() <= r.ymax();
	}

	auto contains(stx::span<float const, 2> r, Point const& p) -> bool
	{
		return contains(Rectangle{{0, 0}, {r[0], r[1]}}, p);
	}

	auto digitize(float value, stx::span<float const> bins) -> std::size_t
	{
		for (auto i = std::size_t{0}; i < bins.size(); ++i) {
			if (value < bins[i])
				return i;
		}
		return std::numeric_limits<std::size_t>::max();
	}

	auto wall_distance(interfaces::IBody const& observer, Rectangle const& walls, float ray_angle) -> float
	{
		auto obs_pos = Point{observer.position().x, observer.position().y};
		auto obs_ori = Vector{observer.orientation()[0], observer.orientation()[1]};

		// Only calculate actual distance when observer is within all walls
		if (!contains(walls, obs_pos)) {
			return 0;
		}

		auto ray_direction = rotate2D(obs_ori, ray_angle);

		auto ray = Ray(obs_pos, Direction(ray_direction));

		auto result = CGAL::intersection(walls, ray);
		if (!result)
			return infinity;

		return boost::apply_visitor(
		   [&](auto&& intersection) {
			   using T = std::decay_t<decltype(intersection)>;

			   if constexpr (std::is_same_v<T, Point>) {
				   // NOTE: Single point intersection treated as no distance at all
				   return 0.f;
			   } else if constexpr (std::is_same_v<T, Segment>) {
				   return std::sqrt(intersection.squared_length());
			   } else {
				   assert(false);
			   }
		   },
		   *result);
	}

	auto is_zero(float val, float epsilon) -> bool
	{
		return std::abs(val) < epsilon;
	}

	auto is_zero(Vector v, float epsilon) -> bool
	{
		return is_zero(v[0], epsilon) && is_zero(v[1], epsilon);
	}

	auto linspace(float min, float max, std::size_t n) -> std::vector<float>
	{
		auto result = std::vector<float>{};
		for (auto i = std::size_t{0}; i <= n - 2; ++i) {
			auto tmp = min + i * (max - min) / std::floor(float(n) - 1.f);
			result.push_back(tmp);
		}
		result.push_back(max);
		return result;
	}

	void centroid_distances(stx::span<float>                                    distances,
	                        interfaces::IBody const&                            observer,
	                        stx::span<std::shared_ptr<interfaces::IBody> const> others,
	                        stx::span<float const>                              sector_limits)
	{
		if (sector_limits.begin() == sector_limits.end()) {
			return;
		}

		auto obs_pos = cv::Vec2f(observer.position());
		auto obs_ori = observer.orientation();

		const auto min = *sector_limits.begin();
		const auto max = *std::prev(sector_limits.end());

		std::fill(distances.begin(), distances.end(), infinity);

		for (decltype(auto) other : others) {
			if (other->uid == observer.uid) {
				continue;
			}

			auto oth_pos = cv::Vec2f(other->position());

			auto towards  = oth_pos - obs_pos;
			auto distance = norm(towards);
			auto angle    = angle2D(obs_ori, towards / distance);

			if (angle < min || max < angle) {
				continue;
			}

			const auto bin = digitize(angle, {std::next(std::begin(sector_limits)), std::size(sector_limits)});
			if (bin == std::numeric_limits<std::size_t>::max()) {
				continue;
			}

			if (distance < distances[bin]) {
				distances[bin] = distance;
			}
		}
	}

	auto calc_locomotion(stx::span<float const, 4> prev_pose, stx::span<float const, 4> curr_pose)
	   -> std::array<float, 2>
	{
		auto const epsilon = 1e-5f;

		auto curr_pos = Vector{curr_pose[0], curr_pose[1]};
		auto curr_ori = Vector{curr_pose[2], curr_pose[3]};

		auto prev_pos = Vector{prev_pose[0], prev_pose[1]};
		auto prev_ori = Vector{prev_pose[2], prev_pose[3]};

		float turn = angle2D(prev_ori, curr_ori);
		if (is_zero(turn, epsilon))
			turn = 0;

		auto forward         = 0.f;
		auto linear_velocity = curr_pos - prev_pos;
		if (!(is_zero(linear_velocity[0], epsilon) && is_zero(linear_velocity[1], epsilon)))
			forward = dot2D(curr_ori, linear_velocity);

		return {turn, forward};
	}

	auto intensity_linear(float d, float max_d) -> float
	{
		if (d < 0 || max_d < d)
			return 0.f;
		return 1.f - d / max_d;
	}

	auto read_all(QFile file) -> std::vector<char>
	{
		Qt5Resources _;

		if (!file.exists())
			throw std::system_error(ENOENT, std::generic_category(), file.fileName().toStdString());
		if (!file.open(QIODevice::ReadOnly))
			throw std::runtime_error(file.errorString().toStdString());

		auto data = std::vector<char>(file.size());
		if (file.read(&data[0], data.size()) == -1)
			throw std::runtime_error(file.errorString().toStdString());
		return data;
	}
}
