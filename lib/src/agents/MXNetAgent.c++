#include <algorithm>
#include <iostream>
#include <random>
#include <map>

#include <robofish/interfaces/IBody.h>
#include <robofish/birghan_ai/agents/MXNetAgent.h++>
#include <robofish/birghan_ai/common.h++>
#include <robofish/birghan_ai/hdf5.h++>

namespace robofish::ai
{

	MXNetAgent::MXNetAgent(stx::span<float const, 2> world,
	                       int                       uid,
	                       stx::span<float const, 2> position,
	                       stx::span<float const, 2> orientation,
	                       Config                    config)
	: Agent{world, uid, position, orientation}
	, raycaster(world, config.far_plane, config.view_of_agents, config.view_of_walls)
	, time_step_(config.time_step)
	, feature_m(config.locomotion.size() + raycaster.view_of_agents().size() + raycaster.view_of_walls().size())
	, symbols_(config.symbols)
	{
		auto ctx = mxnet::cpp::Context::cpu();

		for (auto t : config.locomotion) {
			auto const [min, max, num] = t;
			locomotion_bins.emplace_back(linspace(min, max, num + 1));
		}

		locomotion_m = {&feature_m[0], config.locomotion.size()};

		arguments_["feature"] = {mxnet::cpp::Shape{1, static_cast<mxnet::cpp::index_t>(feature_m.size())}, ctx};
		for (auto [name, array] : config.params) {
			if (array.GetContext() != ctx)
				array = array.Copy(ctx);
			arguments_[name] = array;
		}

		state_names_ = mxnet::cpp::ListRecurrentStates(config.symbols);
		mxnet::cpp::InferRecurrentStates(config.symbols, ctx, &arguments_, arguments_);

		executor = decltype(executor){config.symbols.SimpleBind(ctx, arguments_, {}, {}, {})};

		reset();
	}

	auto MXNetAgent::supported_time_steps() const -> std::vector<std::uint32_t>
	{
		return {time_step_};
	}

	void MXNetAgent::reset()
	{
		std::fill(std::begin(locomotion_m), std::end(locomotion_m), 0);

		for (auto const& name : state_names_) {
			mxnet::cpp::Zero()(name, &arguments_[name]);
		}
	}

	void MXNetAgent::tick(stx::span<std::shared_ptr<interfaces::IBody> const> bodies,
	                      [[maybe_unused]] std::uint32_t                      time_step)
	{
		raycaster(*this, bodies);
		{
			decltype(auto) src = raycaster.view_of_agents();
			std::copy(src.begin(), src.end(), locomotion_m.end());
		}
		{
			decltype(auto) src = raycaster.view_of_walls();
			std::copy(src.begin(), src.end(), locomotion_m.end() + raycaster.view_of_agents().size());
		}

		// Infer computational graph
		arguments_["feature"].SyncCopyFromCPU(&feature_m[0], feature_m.size());
		mxnet::cpp::NDArray::WaitAll();
		executor->Forward(false);

		// Transform locomotion probabilities into continuous locomotion
		for (std::size_t i = 0; i < locomotion_m.size(); ++i) {
			auto probabilities = std::vector<float>(mxnet::cpp::Shape(executor->outputs[i].GetShape()).Size());

			executor->outputs[i].SyncCopyToCPU(probabilities.data(), probabilities.size());

			auto bin        = random.multinomial(probabilities);
			locomotion_m[i] = random.uniform(locomotion_bins[i][bin], locomotion_bins[i][bin + 1]);
		}

		// Save any recurrent output states as inputs for the next inference
		for (std::size_t i = 0; i < std::min(state_names_.size(), executor->outputs.size() - locomotion_m.size()); ++i)
			executor->outputs[locomotion_m.size() + i].CopyTo(&arguments_[state_names_[i]]);

		move_turn_speed(*this, {&locomotion_m[0], locomotion_m.size()}, true);
	}

	auto MXNetAgent::locomotion() const -> stx::span<float const>
	{
		return locomotion_m;
	}

	void MXNetAgent::set_locomotion(stx::span<float const> locomotion)
	{
		if (locomotion.size() != locomotion_m.size())
			throw std::invalid_argument("argument has invalid shape");
		std::copy(locomotion.begin(), locomotion.end(), locomotion_m.begin());
	}

	auto MXNetAgent::state_names() const -> std::vector<std::string> const&
	{
		return state_names_;
	}

	auto MXNetAgent::time_step() const -> std::uint32_t
	{
		return time_step_;
	}

	auto MXNetAgent::arguments() const -> std::map<std::string, mxnet::cpp::NDArray> const&
	{
		return arguments_;
	}

	auto MXNetAgent::symbols() const -> mxnet::cpp::Symbol const&
	{
		return symbols_;
	}

#if HAS_HDF5
	auto MXNetAgent::Config::load(std::string const& filename) -> MXNetAgent::Config
	{
		H5::Exception::dontPrint();
		auto file = H5::H5File{filename, H5F_ACC_RDONLY};

		// All datasets in the root are expected to define an argument for the
		// computational graph
		auto params = hdf5::to<std::map<std::string, mxnet::cpp::NDArray>>(file);

		// The root is expected to be attributed with the following:

		// The JSON-encoded symbols of the computational graph
		if (!file.attrExists("symbols")) {
			throw std::invalid_argument("'symbols' attribute is missing");
		}
		auto symbols = mxnet::cpp::Symbol::LoadJSON(hdf5::to<std::string>(file.openAttribute("symbols")));

		if (!file.attrExists("time_step")) {
			throw std::invalid_argument("'time_step' attribute is missing");
		}
		auto time_step = hdf5::to<std::uint32_t>(file.openAttribute("time_step"));

		if (!file.attrExists("far_plane")) {
			throw std::invalid_argument("'far_plane' attribute is missing");
		}
		auto far_plane = hdf5::to<float>(file.openAttribute("far_plane"));

		if (!file.attrExists("locomotion")) {
			throw std::invalid_argument("'locomotion' attribute is missing");
		}
		auto locomotion = [&]() -> std::array<std::tuple<float, float, int>, 2> {
			auto array = hdf5::to<mxnet::cpp::NDArray>(file.openAttribute("locomotion"));
			{
				auto shape = array.GetShape();
				if (shape.size() != 2) {
					throw std::invalid_argument("'locomotion' attribute has invalid shape");
				}
				if (shape[0] != 2) {
					throw std::invalid_argument("'locomotion' attribute has invalid shape");
				}
				if (shape[1] != 3) {
					throw std::invalid_argument("'locomotion' attribute has invalid shape");
				}
			};
			return {std::make_tuple(array.At(0, 0), array.At(0, 1), static_cast<int>(array.At(0, 2))),
			        std::make_tuple(array.At(1, 0), array.At(1, 1), static_cast<int>(array.At(1, 2)))};
		}();

		auto view_of_agents = std::optional<std::array<float, 2>>();
		if (file.attrExists("view_of_agents")) {
			auto vector = hdf5::to<std::vector<float>>(file.openAttribute("view_of_agents"));
			if (vector.size() != 2) {
				std::invalid_argument("'view_of_agents' attribute - if present - must have size 2");
			}
			view_of_agents = {vector[0], vector[1]};
		}

		auto view_of_walls = std::optional<std::array<float, 2>>();
		if (file.attrExists("view_of_walls")) {
			auto vector = hdf5::to<std::vector<float>>(file.openAttribute("view_of_walls"));
			if (vector.size() != 2) {
				std::invalid_argument("'view_of_walls' attribute - if present - must have size 2");
			}
			view_of_walls = {vector[0], vector[1]};
		}

		return {symbols, params, time_step, locomotion, view_of_agents, view_of_walls, far_plane};
	}
#endif

	void MXNetAgent::seed(RandomState& seed)
	{
		random.seed(seed);
	}
}
