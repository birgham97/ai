
set_target_properties(${target}
PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
)

target_link_libraries(${target} Eigen3::Eigen)
target_link_libraries(${target} Boost::boost Boost::random)
target_link_libraries(${target} CGAL::CGAL)
target_link_libraries(${target} robofish::interfaces::static)

target_sources(${target}
PRIVATE
    common.c++
    RandomState.c++
    Raycaster.c++
    ArrayBody.c++
    Agent.c++
    agents/CouzinAgent.c++
    qt/FileSelect.c++
    robotracker/AgentBehaviorBase.c++
)

target_include_directories(${target} PRIVATE .)

if(USE_HDF5)
    target_link_libraries(${target} hdf5::hdf5_cpp-static)
    target_include_directories(${target} PUBLIC ${HDF5_CXX_INCLUDE_DIRS})
    target_compile_definitions(${target} PUBLIC HAS_HDF5=1)
    target_sources(${target}
    PRIVATE
        hdf5.c++)
endif()

if(USE_MXNET)
    target_link_libraries(${target} MXNet)
    target_compile_definitions(${target} PUBLIC HAS_MXNET=1)
    target_sources(${target}
    PRIVATE
        agents/MXNetAgent.c++
    )
endif()

if(USE_OPENGL)
    target_link_libraries(${target} Epoxy glm Freetype::Freetype harfbuzz::harfbuzz)
    target_compile_definitions(${target} PUBLIC HAS_OPENGL=1)
    target_sources(${target}
    PRIVATE
        opengl/FrameGrabber.c++
        opengl/QuadRenderer.c++
        opengl/RectangleRenderer.c++
        opengl/ViewVectorsRenderer.c++
        opengl/TextRenderer.c++
        opengl/AgentRenderer.c++
        opengl/TracksetRenderer.c++
    )
endif()

if(USE_EGL)
    target_link_libraries(${target} EGL)
    target_compile_definitions(${target} PUBLIC HAS_EGL=1)
    target_sources(${target}
    PRIVATE
        opengl/HeadlessContext.c++
    )
endif()

if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    target_link_libraries(${target} stdc++fs)
    target_compile_options(${target} PRIVATE -W -Wall -Wno-parentheses)
    target_compile_options(${target} PRIVATE -pedantic)
endif()
