#include <robofish/birghan_ai/qt/FileSelect.h++>
#include <robofish/birghan_ai/qt/moc_FileSelect.cpp>

#include <QHBoxLayout>
#include <QPushButton>
#include <QSizePolicy>
#include <QDebug>
#include <QStyleOptionButton>

namespace robofish::ai
{
	static QSize minimum_size(QPushButton* button)
	{
		auto               text_size = button->fontMetrics().size(Qt::TextShowMnemonic, button->text());
		QStyleOptionButton opt;
		opt.initFrom(button);
		opt.rect.setSize(text_size);
		return button->style()->sizeFromContents(QStyle::CT_PushButton, &opt, text_size, button);
	}

	FileSelect::FileSelect(QWidget*             parent,
	                       QString const&       caption,
	                       QString const&       dir,
	                       QString const&       filter,
	                       QString*             selected_filter,
	                       QFileDialog::Options options)
	: QWidget{parent}
	{
		auto layout = new QHBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);

		selected_file_name = new QLineEdit;
		QObject::connect(selected_file_name, &QLineEdit::textEdited, this, &FileSelect::fileSelected);
		layout->addWidget(selected_file_name);

		auto browse          = new QPushButton{"..."};
		auto browse_min_size = minimum_size(browse);
		browse->setMinimumSize(browse_min_size);
		browse->setMaximumSize(browse_min_size);
		browse->setFlat(true);
		layout->addWidget(browse);
		QObject::connect(
		   browse,
		   &QPushButton::clicked,
		   this,
		   [this, caption, dir, filter, selected_filter, options]([[maybe_unused]] bool checked) {
			   auto file_name = QFileDialog::getOpenFileName(this, caption, dir, filter, selected_filter, options);
			   if (file_name.isEmpty()) {
				   return;
			   }
			   this->selected_file_name->setText(file_name);
			   emit this->fileSelected(file_name);
		   });

		setLayout(layout);
	}

	void FileSelect::clear()
	{
		selected_file_name->setText("");
	}
}
